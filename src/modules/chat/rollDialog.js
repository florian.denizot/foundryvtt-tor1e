import {Tor1eRoll} from "../Tor1eRoll.js";

export const tor1eRollDialog = {};

tor1eRollDialog.utilities = {

    "buildRollChatMessage": async function (context, actor, _modifier = 0) {

        let chatOptions = {
            user: game.user.id,
            flavor: context.flavor,
            difficulty: context.difficulty,
            blind: false,
            shadowServant: context.shadowServant,
            modifier: _modifier,
            template: Tor1eRoll.CHAT_TEMPLATE
        };
        let currentHopeValue = actor.system.resources.hope.value;
        //some kind of control ;)
        if (chatOptions.modifier <= 0) {
            ui.notifications.warn(game.i18n.localize("tor1e.roll.warn.noFavouredBonusToAdd"));
            return;
        }
        if (currentHopeValue <= 0) {
            ui.notifications.warn(game.i18n.localize("tor1e.roll.warn.noMoreHopePointToSpend"));
            actor.update({"data.resources.hope.value": 0});
            return;
        }
        let roll = await Tor1eRoll.createModifiedRollBy(chatOptions.modifier, context.roll);
        actor.update({"data.resources.hope.value": currentHopeValue - 1});
        roll.data.canSpendHopePoint = false;

        await roll.render(chatOptions).then(html => {
            chatOptions.content = html;
            chatOptions.flavor = null;
            return ChatMessage.create(chatOptions);
        });

        const fn = actor[context.hopePointPostAction.fn];
        if (fn) await fn.bind(actor)(roll, context.hopePointPostAction.options);
    }
}