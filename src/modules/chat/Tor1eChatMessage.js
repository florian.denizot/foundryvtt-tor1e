export default class Tor1eChatMessage extends ChatMessage {

    static getExetendedDataLocation() {
        return "flags.tor1e";
    }

    static getExtendedData(object = {}, key = "") {
        let fullPath = key !== "" ? `${Tor1eChatMessage.getExetendedDataLocation()}.${key}` : `${Tor1eChatMessage.getExetendedDataLocation()}`;
        return getProperty(object, fullPath);
    }

    async setCombatantState(state) {
        await this.setFlag("tor1e", "state", state);
    }

    getCombatantState() {
        return this.getFlag("tor1e", "state");
    }

    getCombatantTarget() {
        return this.getFlag("tor1e", "target");
    }

    async setCombatantTarget(target) {
        await this.setFlag("tor1e", "target", target);
    }

    getCombatantDamages() {
        return this.getFlag("tor1e", "damages");
    }

    async setCombatantDamages(damages) {
        await this.setFlag("tor1e", "damages", damages);
    }


    /**
     * @override
     * @param data
     * @param options
     */
    update(data, options = {}) {
        super.update(data, options)
    }

    static buildExtendedDataWith(data) {
        return {tor1e: data};
    }

    getExtendedData(key = "") {
        return Tor1eChatMessage.getExtendedData(this, key);
    }

    mergeExtendedDataWith(data) {
        let extendedData = this[Tor1eChatMessage.getExetendedDataLocation()];
        let updateData = {};

        if (!extendedData) {
            updateData = {tor1e: data}
        } else {
            updateData = extendedData;
            mergeObject(updateData, {tor1e: data});
        }

        return updateData;
    }

    updateWithExtendedData(data) {
        let updateData = this.mergeExtendedDataWith(data)

        this.update({"flags": updateData});
    }
}