export class StatusEffects {

    static WOUNDED = "wounded";
    static CONVALESCENT = "convalescent";
    static POISONED = "poisoned";
    static MISERABLE = "miserable";
    static WEARY = "weary";
    static TEMPORARY_MISERABLE = "temporary-miserable";
    static TEMPORARY_WEARY = "temporary-weary";
    static DEAD = "dead";

    static tor1eStatusEffects = [
        {
            id: StatusEffects.WOUNDED,
            label: "tor1e.effects.wounded",
            icon: "systems/tor1e/assets/images/icons/effects/wounded.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.CONVALESCENT,
            label: "tor1e.effects.convalescent",
            icon: "systems/tor1e/assets/images/icons/effects/convalescent.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.POISONED,
            label: "tor1e.effects.poisoned",
            icon: "systems/tor1e/assets/images/icons/effects/poisoned.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.MISERABLE,
            label: "tor1e.effects.miserable",
            icon: "systems/tor1e/assets/images/icons/effects/miserable.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.TEMPORARY_MISERABLE,
            label: "tor1e.effects.temporary-miserable",
            icon: "systems/tor1e/assets/images/icons/effects/temporary-miserable.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.WEARY,
            label: "tor1e.effects.weary",
            icon: "systems/tor1e/assets/images/icons/effects/weary.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
        {
            id: StatusEffects.TEMPORARY_WEARY,
            label: "tor1e.effects.temporary-weary",
            icon: "systems/tor1e/assets/images/icons/effects/temporary-weary.svg",
            flags: {
                tor1e: {
                    "trigger": "endRound",
                    "effectTrigger": "prefillDialog",
                    "script": "",
                    "effectData": {
                        "description": "to be used",
                        "modifier": "to be used"
                    },
                    "secondaryEffect": {
                        "effectTrigger": "targetPrefillDialog",
                        "script": "to be used",
                    },
                    "value": 1
                }
            }
        },
    ];

    static allStatusEffects = StatusEffects.tor1eStatusEffects.concat(CONFIG.statusEffects);

    static onReady() {
        // Create a list of effects from the FVTT System
        CONFIG.tor1e.allEffects = this.allStatusEffects;

        CONFIG.statusEffects = StatusEffects.tor1eStatusEffects.concat(CONFIG.statusEffects);
    }

    static getStatusEffectBy(id){
        return this.allStatusEffects.find(effect => effect.id === id);
    }
}