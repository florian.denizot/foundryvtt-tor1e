export default class SocketHandlers {
    static async updateCombatantStance(data) {
        if (!game.user.isGM) return

        let payload = data?.payload;
        const combat = game.combats.get(payload?.combatId)
        if (!combat || !payload) return

        await combat.updateCombatant(payload);
    }

    static async updateCombatantPoolDice(data) {
        if (!game.user.isGM) return

        let payload = data?.payload;
        const combatant = await fromUuid(payload?.uuid)
        if (!combatant || !payload) return

        await combatant.setPoolDice(payload.flags);
    }
}