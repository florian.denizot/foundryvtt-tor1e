import Tor1eMigration from "./Tor1eMigration.js";
import {Tor1eActor} from "../Tor1eActor.js";

export default class Tor1eMigration0_0_27 extends Tor1eMigration {

    migrationScriptVersion = "0.0.28";

    async migrateWorld() {
        return await super.migrateWorld(this._migrationDataFn, this.migrationScriptVersion, this);
    }

    async _migrationDataFn(migration) {
        let result = false;
        let resultTemp = false;
        console.log("============= MIGRATION 0.0.28 ==============");
        for (let a of game.actors) {
            if (a.type === Tor1eActor.ADVERSARY && migration._needToMigrateAdversaryResourceMax(a)) {
                console.log(`MIGRATION | Actor ${a.name} needs to create and initialize Endurance and Hate max values !`);
                resultTemp = await migration._updateAdversaryResourceMaxData(a);
                result = result && resultTemp;
            } else if (a.type === Tor1eActor.CHARACTER && migration._needToMigrateCharacterResourceMax(a)) {
                console.log(`MIGRATION | Actor ${a.name} needs to create and initialize Endurance and Hope max values !`);
                resultTemp = await migration._updateCharacterResourceMaxData(a);
                result = result && resultTemp;
            } else if (a.type === Tor1eActor.NPC && migration._needToMigrateNPCResourceMax(a)) {
                console.log(`MIGRATION | Actor ${a.name} needs to create and initialize Endurance max value !`);
                resultTemp = await migration._updateNPCResourceMaxData(a);
                result = result && resultTemp;
            }
        }
        console.log("============= FIN MIGRATION 0.0.28 ==============");

        return result;
    }

    // NPC
    _needToMigrateNPCResourceMax(actor) {
        return (this._statNeedToBeMigrated(actor.system.endurance.max));
    }

    async _updateNPCResourceMaxData(actor) {
        console.log(`MIGRATION | Start | Resource Max Values migration for Actor ${actor.name} of type ${actor.type} !`);
        let curEndurance = actor.system.endurance.value;

        let updateData = {
            data: {
                endurance: {
                    max: curEndurance
                }
            },
            token: {
                bar1: {
                    attribute: "endurance"
                }
            }
        }
        await this._updateActor(actor, updateData);
    }

    // CHARACTER
    _needToMigrateCharacterResourceMax(actor) {
        return (this._statNeedToBeMigrated(actor?.system?.resources?.endurance?.max, actor?.system?.resources?.endurance?.rating?.value) || this?._statNeedToBeMigrated(actor?.system?.resources?.hope?.max, actor?.system?.resources?.hope?.rating?.value));
    }

    async _updateCharacterResourceMaxData(actor) {
        console.log(`MIGRATION | Start | Resource Max Values migration for Actor ${actor.name} of type ${actor.type} !`);

        let ratingEndurance = actor?.system?.resources?.endurance?.rating?.value ?? 0;
        let ratingHope = actor?.system?.resources?.hope?.rating?.value ?? 0;

        let updateData = {
            data: {
                resources: {
                    endurance: {
                        max: ratingEndurance
                    },
                    hope: {
                        max: ratingHope
                    }
                }
            },
            token: {
                bar1: {
                    attribute: "resources.endurance"
                },
                bar2: {
                    attribute: "resources.hope"
                }
            }
        }
        await this._updateActor(actor, updateData);
    }

    async _updateActor(actor, updateData) {
        try {
            let result = await actor.update(updateData);
            if (result.name) {
                console.log(`MIGRATION |  ${result.name} with result ${typeof (result) === "object"}`);
            } else {
                console.log(`MIGRATION |  No data migration needed with result ${typeof (result) === "object"}`);
            }
        } catch (e) {
            ui.notifications.error(`MIGRATION |  Error during the migration of ${actor.name} ! `);
            console.error(`MIGRATION |  Error during the migration of ${actor.name}  with updateData :`, updateData);
        }
    }

// ADVERSARY
    _needToMigrateAdversaryResourceMax(actor) {
        return (this._statNeedToBeMigrated(actor.system.endurance.max) || this._statNeedToBeMigrated(actor.system.hate.max));
    }

    _statNeedToBeMigrated(stat, initial) {
        return (stat === undefined || stat === 0) && (initial === undefined || initial !== stat)
    }

    async _updateAdversaryResourceMaxData(actor) {
        console.log(`MIGRATION | Start | Resource Max Values migration for Actor ${actor.name} of type ${actor.type} !`);
        let curEndurance = actor.system.endurance.value;
        let curHate = actor.system.hate.value;

        let updateData = {
            data: {
                endurance: {
                    max: curEndurance
                },
                hate: {
                    max: curHate
                }
            },
            token: {
                bar1: {
                    attribute: "endurance"
                },
                bar2: {
                    attribute: "hate"
                }
            }
        }
        await this._updateActor(actor, updateData);
    }
}