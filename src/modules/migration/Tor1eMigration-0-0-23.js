import Tor1eMigration from "./Tor1eMigration.js";
import {Tor1eActor} from "../Tor1eActor.js";

export default class Tor1eMigration0_0_23 extends Tor1eMigration {

    migrationScriptVersion = "0.0.23";

    async migrateWorld() {
        return await super.migrateWorld(this._migrationDataFn, this.migrationScriptVersion, this);
    }

    async _migrationDataFn(migration) {
        let result = false;

        for (let a of game.actors) {
            if (a.type === Tor1eActor.ADVERSARY && migration._needToMigrateArmour(a) && migration._needToMigrateFaction(a)) {
                console.log(`MIGRATION | Actor ${a.name} needs to migrate for Armour !`);
                result = result && await migration._updateArmourAdversaryData(a);
            }

            if ((a.type === Tor1eActor.ADVERSARY
                || a.type === Tor1eActor.LORE
                || a.type === Tor1eActor.NPC) && migration._needToMigrateFaction(a)) {
                console.log(`MIGRATION | Actor ${a.name} needs to migrate for Faction !`);
                let updateResult = await migration._updateFactionActorData(a);
                result = result && updateResult;
            }
        }

        return result;
    }

    async _updateFactionActorData(actor) {
        console.log(`MIGRATION | Start | Faction migration for Actor ${actor.name} of type ${actor.type} !`);
        let updateFactionData = {
            value: actor.type === Tor1eActor.ADVERSARY,
            type: "Boolean",
            label: "tor1e.actors.stats.faction"
        }
        let result = await actor.update({
            "data.faction": updateFactionData,
        });
        if (result.name) {
            console.log(`MIGRATION |  ${result.name} with result ${typeof (result) === "object"}`);
        } else {
            ui.notifications.info(`MIGRATION |  Error during the migration of ${actor.name} ! `);
            console.log(`MIGRATION |  Error during the migration of ${actor.name} ! `);
        }
    }

    async _updateArmourAdversaryData(actor) {
        let updateData;
        if (!actor.system.armour || !actor.system.armour.favoured) {
            updateData = {
                favoured: {
                    value: false,
                    type: "Boolean",
                    label: "tor1e.stats.favoured"
                }
            }
        } else {
            let armourFavouredData = actor.system.armour.favoured;
            updateData = {
                favoured: {
                    value: armourFavouredData.value || false,
                    type: armourFavouredData.type || "Boolean",
                    label: armourFavouredData.label || "tor1e.stats.favoured"
                }
            }
        }
        let result = await actor.update({
            "data.armour": updateData,
        });
        if (result.name) {
            console.log(`MIGRATION |  ${result.name} with result ${typeof (result) === "object"}`);
        } else {
            ui.notifications.info(`MIGRATION |  Error during the migration of ${actor.name} ! `);
            console.log(`MIGRATION |  Error during the migration of ${actor.name} ! `);
        }
    }

    _needToMigrateArmour(actor) {
        if (!actor.system.armour || !actor.system.armour.favoured) {
            return true;
        }

        let armourFavouredData = actor.system.armour.favoured;
        return !(armourFavouredData !== undefined
            && armourFavouredData.value !== undefined
            && armourFavouredData.label !== undefined
            && armourFavouredData.type !== undefined)
    }

    _needToMigrateFaction(actor) {
        return (actor.system.faction===undefined || actor.system.faction.value===undefined);
    }
}