import Tor1eMigration from "./Tor1eMigration.js";

export default class Tor1eMigration0_0_22 extends Tor1eMigration {

    migrationScriptVersion = "0.0.22";

    async migrateWorld() {
        return await super.migrateWorld(this._migrationDataFn, this.migrationScriptVersion, this);
    }

    async _migrationDataFn(migration) {
        let result = true;
        for (let i of game.items.entities) {
            let newItem = migration._migrateItemData(i)
            if (newItem) {
                result = result && await migration._updateItemData(newItem);
            }
        }

        for (let a of game.actors) {
            for (let i of a.items) {
                let newItem = migration._migrateItemData(i)
                if (newItem) {
                    result = result && await migration._updateOwnedItemData(a, newItem);
                }
            }
        }
        return result;
    }

    async _updateItemData(item) {
        let result = await item.update({
            "data.skill.name": item.name,
        });
        console.log(`MIGRATION |  ${result.name} with result ${typeof (result) === "object"}`)
    }

    async _updateOwnedItemData(a, item) {
        let result = await a.updateEmbeddedDocument('OwnedItem', {
            id: item.id,
            "data.skill.name": item.name,
        });
        console.log(`MIGRATION |  ${result.name} with result ${typeof (result) === "object"}`)
    }

    _migrateItemData(item) {
        if (item.type === "weapon") {

            if (item.system.skill.name === undefined || item.system.skill.id !== undefined) {
                return item;
            }
        }
        return undefined;
    }

}