export const registerSystemSettings = function() {

    const system="tor1e";

    /**
     * Track the system version upon which point a migration was last applied
     */
    game.settings.register(system, "systemMigrationVersion", {
        name: "System Migration Version",
        scope: "world",
        config: false,
        type: String,
        default: ""
    });

}