import {tor1eUtilities} from "./utilities.js";
import * as Dice from "./sheets/dice.js";
import {StatusEffects} from "./effects/status-effects.js";
import Tor1eChatMessage from "./chat/Tor1eChatMessage.js";

/**
 * Extend the base Actor entity.
 * @extends {Actor}
 */
export class Tor1eActor extends Actor {

    static CHARACTER = "character";
    static ADVERSARY = "adversary";
    static NPC = "npc";
    static LORE = "lore";
    static COMMUNITY = "community";

    /**
     * Compute the TN to hit a token.
     * @param token
     * @returns {*}
     * @private
     */
    _computeTN(token) {
        const speaker = ChatMessage.getSpeaker();
        let attacker = game.combat.getActiveCombatants().find(c => c.token.id === speaker.token);
        let constants = CONFIG.tor1e.constants;
        let foe = game.combat.getActiveCombatants().find(c => c.token.id === token.id);
        if (!foe || !foe.actor) {
            ui.notifications.warn(game.i18n.localize("tor1e.combat.warn.noValidCombatTarget"));
            return;
        }
        let foeData = foe.actor;
        let parryBonus = foeData.extendedData.getParryBonus();
        let allEquipedItems = tor1eUtilities.filtering.getAllEquipedItems(foeData.items, foeData.extendedData.isCharacter);
        let shield = tor1eUtilities.filtering.getItemBy(allEquipedItems, constants.armour, constants.shield);
        let shieldBonus
        if (shield) {
            shieldBonus = shield.system.protection.value;
        } else {
            shieldBonus = 0;
        }
        // It's a bit of a hack ... waiting for better ... We pass attacker and opponent to the method
        // because depending on the type of actor, we don't use the same combatant to get the stance so the TN
        let baseTN = this.extendedData.getAttackTn(attacker, foe);
        return baseTN + parryBonus + shieldBonus;
    }

    /**
     * Return THE target of the User or undefined if none or several
     * @returns {undefined|*}
     * @private
     */
    _getTarget() {
        if (game.user.targets && game.user.targets.size === 1) {
            //should be changed  one day with multiple targets ?
            for (let target of game.user.targets) {
                return target;
            }
        }
        return undefined;
    }

    hasFragileHealth() {
        if (!this.extendedData.isCharacter) {
            return true;
        }

        let currentWoundedStatusEffect = this.findStatusEffectById(StatusEffects.WOUNDED);
        let currentTreatedStatusEffect = this.findStatusEffectById(StatusEffects.CONVALESCENT);
        let currentPoisonedStatusEffect = this.findStatusEffectById(StatusEffects.POISONED);

        return !!(currentPoisonedStatusEffect || currentWoundedStatusEffect || currentTreatedStatusEffect);
    }

    async applyAllEffects(statusEffects) {
        return await this.addStatusEffectsBy(statusEffects);
    }

    getWeary() {
        return this.findStatusEffectById(StatusEffects.WEARY) !== undefined || this.findStatusEffectById(StatusEffects.TEMPORARY_WEARY) !== undefined;
    }

    /* -------------------------------------------- */
    async addStatusEffectsBy(statusEffects, options = {renderSheet: false, unique: true}) {
        const statusEffectsToBeAdded = statusEffects
            .filter(statusEffect => !this.hasStatusEffectById(statusEffect) && options.unique === true)

        await this.addStatusEffects(statusEffectsToBeAdded, options);
    }

    /* -------------------------------------------- */
    async addStatusEffectById(id, options = {renderSheet: false, unique: true}) {
        if (this.hasStatusEffectById(id) && options.unique === true) {
            return;
        }
        const statusEffect = CONFIG.statusEffects.find(it => it.id === id);
        await this.addStatusEffect(statusEffect, options);
    }

    /* -------------------------------------------- */
    async addStatusEffects(statusEffects, options = {renderSheet: false, overlay: false}) {
        let effects = statusEffects.map(effect => {
                const result = duplicate(effect);
                result["flags.core.overlay"] = effect.id === "dead";
                result["flags.core.statusId"] = effect.id;
                return result;
            }
        )
        await this.createEmbeddedDocuments("ActiveEffect", effects);
    }

    /* -------------------------------------------- */
    async addStatusEffect(statusEffect, options = {renderSheet: false, overlay: false}) {
        await this.deleteStatusEffectById(statusEffect.id, options);
        const effect = duplicate(statusEffect);

        this.createEmbeddedDocuments("ActiveEffect", [{
            "flags.core.statusId": effect.id,
            "flags.core.overlay": options.overlay,
            label: game.i18n.localize(effect.label),
            icon: effect.icon,
            origin: this.uuid,
        }]);
    }

    /* -------------------------------------------- */
    async toggleStatusEffectById(id, options = {renderSheet: true}) {
        const effect = this.findStatusEffectById(id);
        if (effect) {
            await this.deleteStatusEffectById(id);
        } else {
            await this.addStatusEffectById(id, options)
        }
    }

    /* -------------------------------------------- */
    hasStatusEffectById(id) {
        const effects = this.findStatusEffectById(id);
        return (effects !== undefined);
    }

    /* -------------------------------------------- */
    buildStatusEffectById(id, options = {renderSheet: true}) {
        const buildEffect = this.findStatusEffectById(id);
        const effectInConfiguration = Array.from(StatusEffects.allStatusEffects?.values())
            .find(
                it => it.id === id
            );
        return {
            value: (buildEffect !== undefined),
            icon: effectInConfiguration.icon,
            label: effectInConfiguration.label
        }
    }

    /* -------------------------------------------- */
    findStatusEffectById(id) {
        return Array.from(this.effects?.values())
            .find(it => it.flags.core?.statusId === id);
    }

    /* -------------------------------------------- */
    async deleteStatusEffectById(id, options = {renderSheet: true}) {
        const effects = Array.from(this.effects?.values())
            .filter(it => it.flags.core?.statusId === id);
        await this._deleteStatusEffects(effects, options);
    }

    /* -------------------------------------------- */
    async _deleteStatusEffects(effects, options) {
        await this._deleteStatusEffectsByIds(effects.map(it => it.id), options);
    }

    /* -------------------------------------------- */
    async _deleteStatusEffectsByIds(effectIds, options) {
        await this.deleteEmbeddedDocuments('ActiveEffect', effectIds, options);
    }

    /* -------------------------------------------- */
    async attackOpponentWith(weaponName, options = {automaticDifficultyRoll: false}) {
        let targetToken = this._getTarget();
        if (targetToken === undefined) {
            ui.notifications.warn(game.i18n.localize("tor1e.combat.warn.noValidTarget"));
            return;
        }

        let weaponUsed = this.extendedData.getWeaponUsed(weaponName);
        if (weaponUsed === undefined) {
            ui.notifications.warn(
                game.i18n.format("tor1e.combat.warn.noValidWeaponEquipped", {skill: weaponName})
            );
            return;
        }

        let tn = this._computeTN(targetToken);
        if (!tn) return;
        let item = this.extendedData.getItemFrom(weaponUsed.name, weaponUsed.type);
        let shadowServant = this.extendedData.isHostile;
        let actionValue = item.value;
        let modifier = item.modifier;
        let isFavoured = item.isFavoured;
        let optionData = {
            displayRoll: true,
            weapon: weaponUsed,
            hostile: shadowServant,
            target: {
                id: targetToken.id,
                name: targetToken.name,
            },
            difficulty: tn,
        }
        let roll = await Dice.taskCheck({
            actor: this,
            user: game.user,
            difficulty: tn,
            askForOptions: !options.automaticDifficultyRoll,
            actionValue: actionValue,
            actionName: weaponName,
            wearyRoll: this.getWeary(),
            modifier: modifier,
            hopeModifier: this.extendedData.getHopeModifier("body", isFavoured),
            shadowServant: shadowServant,
            hopePoint: tor1eUtilities.utilities.try(() => this.system.resources.hope.value, 0),
            hopePointPostAction: {
                fn: "createAttackResultChatMessage",
                options: optionData,
            },
        });

        if (!roll || roll.isFailure()) {
            return;
        }
        await this.createAttackResultChatMessage(roll, optionData);
    }

    async createBattleRollResultChatMessage(roll, options = {}) {
        if (options.displayRoll && game.dice3d) {
            game.dice3d.showForRoll(roll);
        }

        const message = game.i18n.format("tor1e.combat.chat.preparation-roll.message", {successDiceBonus: roll.customResult.result.type.nbBonusDice})

        let cardData = {
            tokenId: options.token.id,
            owner: options.actorId,
            roll: roll,
            difficulty: options.difficulty,
            actorName: options.actorName,
            message: message
        };

        let combat = game.combats.get(options.combatId);

        let chatData = {
            user: game.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        chatData = mergeObject(chatData, {
            content: await renderTemplate("systems/tor1e/templates/chat/combat-preparation-roll-card.hbs", cardData),
            flags: Tor1eChatMessage.buildExtendedDataWith(cardData),
        });

        await ChatMessage.create(chatData);

        await combat._updateCombatantPoolDice(roll, options.token.id);
    }

    async createAttackResultChatMessage(roll, options = {}) {
        if (options.displayRoll && game.dice3d) {
            game.dice3d.showForRoll(roll);
        }

        //create chat message for opponent to ake damage, roll protection, ...
        let actorBonusDamage
        let weaponUsed = this.items.get(options.weapon.id || options.weapon._id);
        if (weaponUsed.system.ranged.value) {
            actorBonusDamage = this.extendedData.getRangeBonusDamage();
        } else {
            actorBonusDamage = this.extendedData.getCloseBonusDamage();
        }
        let damages = roll.customResult.result.type.computeDamage(weaponUsed.system.damage.value, actorBonusDamage);

        let featDieInstance = roll.dice
            .find(die =>
                (die.constructor.name === "TORFeatBaseDie")
                || (die.constructor.name === "TORSauronicFeatBaseDie"));
        let cls = featDieInstance.constructor;
        let featDieResult = cls.getResultValue(featDieInstance.results,
            {
                shadowServant: options.hostile,
                bestFeatDie: roll.data.bestFeatDie
            }
        )
        let piercingShot = featDieResult >= weaponUsed.system.edge.value;

        let chatData = {
            user: game.user.id,
            speaker: ChatMessage.getSpeaker()
        };

        let weaponDamageDetailMessage = game.i18n.format("tor1e.combat.chat.details.damage.weapon",
            {weaponDamage: damages.weapon,});
        let bonusDamageDetailMessage = game.i18n.format("tor1e.combat.chat.details.damage.bonus",
            {bonusDamage: damages.bonus,});
        let piercingShotDescriptionMessage = piercingShot ? game.i18n.format("tor1e.combat.chat.piercing.description",
            {
                targetName: options.target.name,
                weaponInjuryValue: weaponUsed.system.injury.value,
            }) : "";

        let resultDescriptionMessage = game.i18n.format("tor1e.combat.chat.result.paragraph",
            {
                attackerName: this.name,
                weaponName: weaponUsed.name,
                targetName: options.target.name,
                totalDamage: damages.total
            });
        let cardData = {
            owner: options.target.id,
            roll: roll,
            difficulty: options.difficulty,
            attacker: {name: this.name},
            target: {
                name: options.target.name,
                id: options.target.id
            },
            weapon: options.weapon,
            piercingShot: piercingShot,
            damages: damages,
            state: {
                ooc: false,
                wearyApplied: false,
                effects: false,
                damageDealt: false,
                protectionRolled: false,
            },
            resultDescription: resultDescriptionMessage,
            weaponDamageDetail: weaponDamageDetailMessage,
            bonusDamageDetail: bonusDamageDetailMessage,
            piercingShotDescription: piercingShotDescriptionMessage,
        };

        chatData = mergeObject(chatData, {
            content: await renderTemplate("systems/tor1e/templates/chat/combat-damage-card.hbs", cardData),
            flags: Tor1eChatMessage.buildExtendedDataWith(cardData),
        });

        await ChatMessage.create(chatData);
    }

    async rerollProtectionRoll(roll, options) {
        if (options.displayRoll && game.dice3d) {
            game.dice3d.showForRoll(roll);
        }

        const message = game.messages.get(options.messageId);
        let data = Tor1eChatMessage.getExtendedData(message);
        data = mergeObject(data, {
            protectionRolled: options.protectionRolled,
            isWounded: roll.isFailure(),
            effects: options.effects,
        });

        await renderTemplate("systems/tor1e/templates/chat/combat-damage-card.hbs", data).then(html => {
            message.update({content: html})
        });

        message.setFlags = Tor1eChatMessage.buildExtendedDataWith(data);
    }

    /**
     * Augment the basic actor data with additional dynamic data.
     * @override
     */
    prepareData() {
        super.prepareData();

        const actorData = this;
        let extendedData = {};

        if (actorData.type === "adversary") extendedData = this._prepareAdversaryData(this);
        if (actorData.type === "character") extendedData = this._prepareCharacterData(this);
        if (actorData.type === "npc") extendedData = this._prepareNpcData(this);
        if (actorData.type === "lore") extendedData = this._prepareLoreData(this);

        extendedData.state = {
            weary: this.buildStatusEffectById(StatusEffects.WEARY),
            temporaryWeary: this.buildStatusEffectById(StatusEffects.TEMPORARY_WEARY),
            temporaryMiserable: this.buildStatusEffectById(StatusEffects.TEMPORARY_MISERABLE),
            miserable: this.buildStatusEffectById(StatusEffects.MISERABLE),
            poisoned: this.buildStatusEffectById(StatusEffects.POISONED),
            wounded: this.buildStatusEffectById(StatusEffects.WOUNDED),
            convalescent: this.buildStatusEffectById(StatusEffects.CONVALESCENT),
        }

        this.extendedData = extendedData;
    }

    _prepareCharacterData(actor) {
        let constants = CONFIG.tor1e.constants;
        let actorData = actor;
        return {
            isCharacter: true,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: false,
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 0
                } else {
                    return 1000;
                }
            },
            getInitiativeBonus() {
                return actor.system.attributes.wits.value;
            },
            getAttackTn(attacker, target) {
                return attacker.getStance()?.difficulty;
            },
            _getEncumbrance() {
                return tor1eUtilities.filtering.getEncumbrance(actorData.items);
            },
            getFatigue() {
                return actorData.system.resources.travelFatigue.value + this._getEncumbrance();
            },
            getEndurance() {
                return actorData.system.resources.endurance.value;
            },
            async updateEndurance(value) {
                let newEndurance = value < 0 ? 0 : value;
                return await actor.update({"data.resources.endurance.value": newEndurance});
            },
            getParryBonus() {
                return actorData.system.combatAttributes.parry.value;
            },
            getProtectionRollModifier() {
                return this.getHeadGearProtectionValue();
            },
            getHeadGearProtectionValue() {
                let allEquipedItems = tor1eUtilities.filtering.getAllEquipedItems(actorData.items, true);
                let result = tor1eUtilities.filtering
                    .getItemIn(allEquipedItems, constants.armour, [constants.headgear]);
                return (result && result.system.protection.value) || 0;
            },
            getArmourProtectionValue() {
                let allEquipedItems = tor1eUtilities.filtering.getAllEquipedItems(actorData.items, true);
                let result = tor1eUtilities.filtering
                    .getItemIn(allEquipedItems, constants.armour, [constants.mailArmour, constants.leatherArmour]);
                return (result && result.system.protection.value) || 0;
            },
            canSpendHopePoint() {
                return actorData.system.resources.hope.value;
            },
            getWeaponUsed(weaponName) {
                let allEquipedItems = tor1eUtilities.filtering.getAllEquipedItems(actorData.items, true);
                return tor1eUtilities.filtering
                    .getItemsBy(allEquipedItems, constants.weapon)
                    .find(weapon => weapon.name === weaponName);
            },
            getRangeBonusDamage() {
                return actorData.system.combatAttributes.damage.ranged.value;
            },
            getCloseBonusDamage() {
                return actorData.system.combatAttributes.damage.value;
            },
            getHopeModifier() {
                return actorData.system.attributes.body.value;
            },
            getHopePoint() {
                return actorData.system.resources.hope.value;
            },
            getItemFrom(_name, _type) {
                if (!actor) return null;

                // We get the weapon from the actor
                let _item = actor.items
                    .find(i =>
                        i.name === _name && i.type === _type);
                if (!_item) return _item;
                let itemData = _item.system;
                // We get the skill name store in the weapon
                if (!itemData || !itemData.skill) return _item;
                let skillName = itemData.skill.name;
                // We get the skill from the skill name
                let _weaponSkill = actor.items
                    .find(i =>
                        i.name.toLowerCase() === skillName.toLowerCase() && i.type.toLowerCase() === "skill".toLowerCase());
                //we try to find a cultural skill
                let culturalWeaponSkillName = `(${game.i18n.localize(itemData.group.value)})`;
                let _culturalWeaponSkill = actor.items
                    .find(i =>
                        i.name.toLowerCase() === culturalWeaponSkillName.toLowerCase() && i.type === "skill");

                // if no skill is found we return as it is
                if (!_weaponSkill && !_culturalWeaponSkill) return _item;

                //we try to find the best skill, the one with the bigger value
                let _weaponSkillValue = _weaponSkill ? _weaponSkill.system.value : 0;
                let _culturalWeaponSkillValue = _culturalWeaponSkill ? _culturalWeaponSkill.system.value : 0;
                let _skill = {};
                if (_weaponSkill && _culturalWeaponSkill) {
                    _skill = (_culturalWeaponSkillValue >= _weaponSkillValue) ? _culturalWeaponSkill : _weaponSkill;
                } else if (_weaponSkill) {
                    _skill = _weaponSkill;
                } else {
                    _skill = _culturalWeaponSkill;
                }

                let skillData = _skill.system;
                _item.value = skillData.value;
                _item.modifier = 0;
                _item.favouredSkillValue = skillData.favoured.value ? actorData.system.attributes.body.value : 0;
                return _item;
            },
        }
    }

    _prepareNpcData(actor) {
        return {
            isCharacter: false,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: true,
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 0
                } else {
                    return 1000;
                }
            },
            getInitiativeBonus() {
                return actor.system.attributeLevel.value;
            },
            getParryBonus: function () {
                return 0;
            },
            getItemFrom(_name, _type) {
                let _item = actor ? actor.items
                    .find(i =>
                        i.name === _name && i.type === _type) : null;

                if (!_item) {
                    ui.notifications.error(game.i18n.format("tor1e.combat.error.itemNotFound"),
                        {
                            name: _name,
                            type: "_type"
                        })
                    return _item;
                }

                let itemData = _item.system;
                if (!itemData.skill) {
                    ui.notifications.warn(game.i18n.format("tor1e.combat.warn.incompleteItem"),
                        {
                            item: _name,
                            element: "skill"
                        })
                    return _item;
                }

                _item.value = itemData.skill.value;
                _item.isFavoured = itemData.skill.favoured.value;
                _item.modifier = itemData.skill.favoured.value ? actor.extendedData.getAttributeLevel() : 0;
                return _item;
            }
        }
    }

    _prepareLoreData(actor) {
        return {
            isCharacter: false,
            isFriendly: true,
            isHostile: false,
            isRenownCharacter: true,
            getParryBonus: function () {
                return 0;
            },
        }
    }

    _prepareAdversaryData(actor) {
        let constants = CONFIG.tor1e.constants;
        let actorData = actor;
        return {
            isCharacter: false,
            isFriendly: false,
            isHostile: true,
            isRenownCharacter: false,
            getRoleBonus(pcsAreAttacking) {
                if (pcsAreAttacking) {
                    return 1000;
                } else {
                    return 0
                }
            },
            getInitiativeBonus() {
                return actor.system.attributeLevel.value;
            },
            getAttackTn(attacker, target) {
                return target.getStance()?.difficulty;
            },
            getAttributeLevel() {
                return actorData.system.attributeLevel.value;
            },
            getEndurance: function () {
                return actorData.system.endurance.value;
            },
            updateEndurance: async function (value) {
                let newEndurance = value < 0 ? 0 : value;
                return await actor.update({"data.endurance.value": newEndurance});
            },
            getArmourProtectionValue: function () {
                let result = tor1eUtilities.filtering
                    .getItemIn(actorData.items, constants.armour, [constants.mailArmour, constants.leatherArmour]);
                return (result && result.system.protection.value) || 0;
            },
            getParryBonus: function () {
                return actorData.system.parry.value;
            },
            getHeadGearProtectionValue: function () {
                let result = tor1eUtilities.filtering
                    .getItemIn(actorData.items, constants.armour, [constants.headgear]);
                return (result && result.system.protection.value) || 0;
            },
            getProtectionRollModifier: function () {
                let favouredBonus = actorData.system.armour.favoured.value ? actor.extendedData.getAttributeLevel(actorData) : 0;
                let headGearProtectionValue = this.getHeadGearProtectionValue();
                return favouredBonus + headGearProtectionValue;
            },
            getWeaponUsed(weaponName) {
                let constants = CONFIG.tor1e.constants;
                return tor1eUtilities.filtering
                    .getItemsBy(actorData.items, constants.weapon)
                    .find(weapon => weapon.name === weaponName);
            },
            getRangeBonusDamage() {
                return actor.extendedData.getAttributeLevel(actorData);
            },
            getCloseBonusDamage() {
                return actor.extendedData.getAttributeLevel(actorData);
            },
            getHopeModifier() {
                return 0;
            },
            getHopePoint() {
                return 0;
            },
            getItemFrom(_name, _type) {
                let _item = actor ? actor.items
                    .find(i =>
                        i.name === _name && i.type === _type) : null;

                if (!_item) {
                    ui.notifications.error(game.i18n.format("tor1e.combat.error.itemNotFound"),
                        {
                            name: _name,
                            type: "_type"
                        })
                    return _item;
                }

                let itemData = _item.system;
                if (!itemData.skill) {
                    ui.notifications.warn(game.i18n.format("tor1e.combat.warn.incompleteItem"),
                        {
                            item: _name,
                            element: "skill"
                        })
                    return _item;
                }

                _item.value = itemData.skill.value;
                _item.isFavoured = itemData.skill.favoured.value;
                _item.modifier = itemData.skill.favoured.value ? actor.extendedData.getAttributeLevel() : 0;
                return _item;
            }

        }
    }
}