function _isMacroExists(macroName, macroContent, icon = undefined) {
    const entities = Array.from(game.macros);
    if (icon) {
        return entities.find(
            m => (m.name === macroName) && (m.command === macroContent) && (icon === m.img));
    } else {
        return entities.find(
            m => (m.name === macroName) && (m.command === macroContent)
        );
    }
}

async function _createMacro(itemName, itemImg, command) {
    return await Macro.create({
        name: itemName,
        type: "script",
        img: itemImg,
        command: command,
        permission: {default: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER}
    }, {displaySheet: false});
}

async function createMacro(macro, document, command, slot) {
    if (!macro) {
        macro = await _createMacro(document.name, document.img, command);
    }
    game.user.assignHotbarMacro(macro, slot);
}

export default function () {
    /**
     * Create a macro when dropping an entity on the hotbar
     * Item      - open roll dialog for item
     * Actor     - open actor sheet
     * Journal   - open journal sheet
     */
    Hooks.on("hotbarDrop", (bar, data, slot) => {
        // Create item macro if rollable item - weapon, spell, prayer, trait, or skill
        if (data.type === "Item") {
            const item = fromUuidSync(data.itemId);
            let itemType = item?.type ?? data?.type;
            let itemName = item?.name ?? data?.name;
            let itemImg = item?.img ?? data?.img;
            if (itemType !== "weapon" && itemType !== "trait" && itemType !== "skill")
                return;
            if (itemType === "weapon") {

                let command = `game.tor1e.macro.utility.rollItemMacro("${itemName}", "${itemType}");`;
                let macro = _isMacroExists(itemName, command);
                createMacro(macro, item, command, slot);
                return false;
            }
        }
        // Create a macro to open the actor sheet of the actor dropped on the hotbar
        else if (data.type === "Actor") {
            const actor = fromUuidSync(data.uuid);
            let command = `fromUuidSync("${data.uuid}").sheet.render(true)`
            let macro = _isMacroExists(actor.name, command);
            createMacro(macro, actor, command, slot);
            return false;
        }
        // Create a macro to open the journal sheet of the journal dropped on the hotbar
        else if (data.type === "JournalEntry") {
            let journal = fromUuidSync(data.uuid);
            let command = `fromUuidSync("${data.uuid}").sheet.render(true)`
            let macro = _isMacroExists(journal.name, command);
            createMacro(macro, journal, command, slot);
            return false;
        }
        return true;
    });
}
