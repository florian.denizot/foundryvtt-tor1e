export default class Tor1eItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "item"],
            width: 500,
            height: 550
        });
    }

    get template() {
        return `systems/tor1e/templates/sheets/items/${this.item.type}-sheet.hbs`
    }

    async getData() {
        const baseData = super.getData();
        let item = baseData.item;

        return {
            description: await TextEditor.enrichHTML(this.object.system.description.value, {async: true}),
            owner: this.item.isOwner,
            config: CONFIG.tor1e,
            editable: this.isEditable,
            item: item,
            system: item.system,
            backgroundImages: CONFIG.tor1e.backgroundImages[`${this.item.type}`]
        };
    }
}