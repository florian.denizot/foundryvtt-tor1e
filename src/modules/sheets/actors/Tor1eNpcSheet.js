import {tor1eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor1eNpcSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 500,
            height: 725,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/nonplayercharacter-sheet.hbs`
        });
    }

    async getData() {
        const baseData = super.getData();
        let constants = CONFIG.tor1e.constants;

        let items = baseData.items.map(i => this.actor.items.get(i._id));

        return {
            description: await TextEditor.enrichHTML(this.object.system.description.value, {async: true}),
            owner: this.actor.isOwner,
            system: baseData.actor.system,
            actor: baseData.actor,
            config: CONFIG.tor1e,
            backgroundImages: CONFIG.tor1e.backgroundImages["npc"],
            specialities: tor1eUtilities.filtering.getItemsBy(items, constants.trait, constants.speciality),
            distinctiveFeatures: tor1eUtilities.filtering.getItemsBy(items, constants.trait, constants.distinctiveFeature),
            flaws: tor1eUtilities.filtering.getItemsBy(items, constants.trait, constants.flaw),
            skills: tor1eUtilities.filtering.getItemsNot(items, constants.skill, constants.combat),
            weaponSkills: tor1eUtilities.filtering.getItemsBy(items, constants.skill, constants.combat),
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
            }
        };
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor1eEffect").click(tor1eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));

        // Owner-only listeners
        if (this.actor.isOwner) {
            let extra = {
                "favouredSkillValue": this.actor.system.attributeLevel.value
            }
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));
            html.find(".item-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, extra));
            html.find(".skill-name").click(tor1eUtilities.eventsProcessing.onSkillName.bind(this, extra));

        }

        super.activateListeners(html);
    }

}