import {tor1eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor1eCharacterSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 800,
            height: 800,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/character-sheet.hbs`
        });
    }

    /**
     *
     * @param xs
     * @param key
     * @returns {*}
     * @private
     */
    _subgroupBy = function (xs, key) {
        return (xs || []).reduce(function (map, item) {
            (map[item.system[key].value] = map[item.system[key].value] || []).push(item);
            return map;
        }, {});
    };

    /**
     *
     * @param xs
     * @param key
     * @returns {*}
     * @private
     */
    _groupBy = function (xs, key) {
        return (xs || []).reduce(function (map, x) {
            (map[x[key]] = map[x[key]] || []).push(x);
            return map;
        }, {});
    };

    /**
     * Take an array of values and split it in 2 part depending on the index in the array
     * @returns {*[]}
     * @private
     * @param xs
     */
    _splitInTwo(xs) {
        let lefts = (xs || []).filter((v, i) => !(i % 2));
        let rights = (xs || []).filter((v, i) => i % 2);
        return [lefts, rights]
    }

    _getEquipped(items) {
        return (items || []).filter(function (item) {
            return item.system.equipped && item.system.equipped.value === true;
        });
    }

    _first(items) {
        if (!items) return;
        return items.find(Boolean);
    }

    async getData() {
        const baseData = super.getData();
        let constants = CONFIG.tor1e.constants;
        let items = baseData.items.map(i => this.actor.items.get(i._id));
        const ownedItems = this._groupBy(items, 'type');
        const traits = this._subgroupBy(ownedItems[constants.trait], 'group');
        const skills = this._subgroupBy(ownedItems[constants.skill], 'group');
        const armours = this._subgroupBy(this._getEquipped(ownedItems[constants.armour]), 'group');
        let [leftMiscItem, rightMiscItem] = this._splitInTwo(ownedItems[constants.miscellaneous]);
        let headgear = this._first(armours[constants.headgear]);
        let shield = this._first(armours[constants.shield]);
        let armour = this._first(armours[constants.mailArmour]) || this._first(armours[constants.leatherArmour]);
        let encumbrance = tor1eUtilities.filtering.getEncumbrance(items);
        let currentFatigue = parseInt(this.actor.system.resources.travelFatigue.value) + encumbrance;
        let currentShadow = parseInt(this.actor.system.resources.shadow.permanent.value) + parseInt(this.actor.system.resources.shadow.temporary.value);

        return {
            background: await TextEditor.enrichHTML(this.object.system.history.background.value, {async: true}),
            company: await TextEditor.enrichHTML(this.object.system.history.company.value, {async: true}),
            fellowshipPhase: await TextEditor.enrichHTML(this.object.system.history.fellowshipPhase.value, {async: true}),
            taleOfYears: await TextEditor.enrichHTML(this.object.system.history.taleOfYears.value, {async: true}),
            notes: await TextEditor.enrichHTML(this.object.system.history.notes.value, {async: true}),
            owner: this.actor.isOwner,
            model: baseData.actor.system,
            actor: baseData.actor,
            config: CONFIG.tor1e,
            encumbrance: encumbrance,
            backgroundImages: CONFIG.tor1e.backgroundImages["character"],
            specialities: traits[constants.speciality],
            distinctiveFeatures: traits[constants.distinctiveFeature],
            flaws: traits[constants.flaw],
            weaponSkills: skills[constants.combat],
            rewards: ownedItems[constants.reward],
            virtues: ownedItems[constants.virtues],
            armours: ownedItems[constants.armour],
            weapons: ownedItems[constants.weapon],
            leftMiscItem: leftMiscItem,
            rightMiscItem: rightMiscItem,
            shieldDTO: {
                id: shield ? shield.id : 0,
                name: shield ? shield.name : game.i18n.localize("tor1e.actors.stats.noShield"),
                value: shield ? shield.system.protection.value : 0,
            },
            headgearDTO: {
                id: headgear ? headgear.id : 0,
                name: headgear ? headgear.name : game.i18n.localize("tor1e.actors.stats.noHeadgear"),
                value: headgear ? headgear.system.protection.value : 0,
            },
            armourDTO: {
                id: armour ? armour.id : 0,
                css: "dice",
                name: armour ? armour.name : game.i18n.localize("tor1e.actors.stats.noArmour"),
                value: armour ? armour.system.protection.value : 0,
                favoured: {
                    state: false,
                    name: "data.armour.favoured.value",
                    value: 0
                },
                roll: {
                    label: "tor1e.rolls.protection",
                    associatedAttribute: "body",
                    bonus: headgear ? headgear.system.protection.value : 0
                }
            },
            fatigue: {
                "value": currentFatigue,
                "css": this._shouldWarnPlayer(currentFatigue, this.actor.system.resources.endurance.value, this.actor.findStatusEffectById(StatusEffects.WEARY)) ? "warn" : ""
            },
            shadow: {
                "value": currentShadow,
                "css": this._shouldWarnPlayer(currentShadow, this.actor.system.resources.hope.value, this.actor.findStatusEffectById(StatusEffects.MISERABLE)) ? "warn" : ""
            },
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "temporary-weary": this.actor.buildStatusEffectById(StatusEffects.TEMPORARY_WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "convalescent": this.actor.buildStatusEffectById(StatusEffects.CONVALESCENT),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
                "miserable": this.actor.buildStatusEffectById(StatusEffects.MISERABLE),
                "temporary-miserable": this.actor.buildStatusEffectById(StatusEffects.TEMPORARY_MISERABLE),
            }
        };
    }

    /**
     * Sets up the data transfer within a drag and drop event. This function is triggered
     * when the user starts dragging an inventory item, and dataTransfer is set to the
     * relevant data needed by the _onDrop function. See that for how drop events
     * are handled.
     *
     * @private
     *
     * @param {Object} event    event triggered by item dragging
     */
    _onDragWeaponStart(event) {
        let itemId = event.currentTarget.getAttribute("data-item-id");
        const initialItem = this.actor.items.get(itemId);
        const item = duplicate(initialItem)
        event.dataTransfer.setData("text/plain", JSON.stringify({
            type: "Item",
            actorId: this.actor.id,
            data: item,
            itemId: initialItem.uuid,
        }));
    }

    _shouldWarnPlayer(badStat, goodStat, state) {
        return badStat > goodStat && !state;
    }

    activateListeners(html) {

        // Combat Dragging
        let combatSkillHandler = ev => this._onDragWeaponStart(ev);
        html.find('.item-draggable').each((i, li) => {
            li.setAttribute("draggable", true);
            li.addEventListener("dragstart", combatSkillHandler, false);
        });

        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor1eEffect").click(tor1eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));

        // Owner-only listeners
        if (this.actor.isOwner) {
            html.find(".inline-3-states-modify").click(tor1eUtilities.eventsProcessing.onThreeStatesModify.bind(this, {}));
            html.find(".inline-skill-modify").click(tor1eUtilities.eventsProcessing.onSkillModify.bind(this, {}));
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));

            html.find(".skill-name").click(tor1eUtilities.eventsProcessing.onSkillName.bind(this, {}));

            html.find(".item-skill-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, {}));
            html.find(".weapon-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, {}));

        }

        super.activateListeners(html);
    }

}