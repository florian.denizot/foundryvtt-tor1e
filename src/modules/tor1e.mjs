import {tor1e} from "./config.js";
import Tor1eItemSheet from "./sheets/items/Tor1eItemSheet.js";
import Tor1eCommunitySheet from "./sheets/actors/Tor1eCommunitySheet.js";
import Tor1eLoreSheet from "./sheets/actors/Tor1eLoreSheet.js";
import Tor1eNpcSheet from "./sheets/actors/Tor1eNpcSheet.js";
import Tor1eAdversarySheet from "./sheets/actors/Tor1eAdversarySheet.js";
import Tor1eCharacterSheet from "./sheets/actors/Tor1eCharacterSheet.js";
import {TORFeatBaseDie, TORSauronicFeatBaseDie, TORSuccessDie, TORWearySuccessDie} from "./die.js";
import Tor1eItem from "./Tor1eItem.js";
import {Tor1eRoll} from "./Tor1eRoll.js";
import Tor1eCombat from "./combat/Tor1eCombat.js";
import Tor1eCombatTracker from "./combat/Tor1eCombatTracker.js";
import registerHooks from "./system/hooks.js"
import {Tor1eActor} from "./Tor1eActor.js";
import {tor1eUtilities} from "./utilities.js";
import {registerSystemSettings} from "./settings/settings.js";
import Tor1eMigration from "./migration/Tor1eMigration.js";
import Tor1eMigration0_0_22 from "./migration/Tor1eMigration-0-0-22.js";
import Tor1eMigration0_0_23 from "./migration/Tor1eMigration-0-0-23.js";
import Tor1eMigration0_0_28 from "./migration/Tor1eMigration-0-0-28.js";
import {StatusEffects} from "./effects/status-effects.js";
import activateSocketListener from "./system/socket.js";
import Tor1eChatMessage from "./chat/Tor1eChatMessage.js";
import Tor1eCombatant from "./combat/Tor1eCombatant.js";
import Tor1eCombatantConfig from "./combat/Tor1eCombatantConfig.js";


Hooks.once("init", async function () {
    console.log("TOR1E | Initializing The One Ring 1st edition system.");

    game.tor1e = {
        macro: {
            utility: tor1eUtilities.macro,
        }
    }

    CONFIG.tor1e = tor1e;

    // Define custom Roll class (change with attention because of compatibility issue with Dice Cheater Protector module)
    CONFIG.Dice.rolls.push(CONFIG.Dice.rolls[0]);
    CONFIG.Dice.rolls[0] = Tor1eRoll;

    CONFIG.Actor.documentClass = Tor1eActor;
    CONFIG.ChatMessage.documentClass = Tor1eChatMessage;
    CONFIG.Item.documentClass = Tor1eItem;
    CONFIG.Combat.documentClass = Tor1eCombat;
    CONFIG.Combatant.documentClass = Tor1eCombatant;
    CONFIG.Combatant.sheetClass = Tor1eCombatantConfig;
    CONFIG.ui.combat = Tor1eCombatTracker;

    CONFIG.tor1e.STANDARD_RESULTS = {
        1: {
            label: `<img src="systems/tor1e/assets/images/dice/s_1.png" alt="{{localize tor1e.dice.standard.1}}" />`,
            order: 1,
            result: 1
        },
        2: {
            label: `<img src='systems/tor1e/assets/images/dice/s_2.png'  alt="{{localize tor1e.dice.standard.2}}" />`,
            order: 2,
            result: 2
        },
        3: {
            label: `<img src='systems/tor1e/assets/images/dice/s_3.png'  alt="{{localize tor1e.dice.standard.3}}" />`,
            order: 3,
            result: 3
        },
        4: {
            label: `<img src='systems/tor1e/assets/images/dice/s_4.png'  alt="{{localize tor1e.dice.standard.4}}" />`,
            order: 4,
            result: 4
        },
        5: {
            label: `<img src='systems/tor1e/assets/images/dice/s_5.png'  alt="{{localize tor1e.dice.standard.5}}" />`,
            order: 5,
            result: 5
        },
        6: {
            label: `<img src='systems/tor1e/assets/images/dice/s_6.png'  alt="{{localize tor1e.dice.standard.6}}" />`,
            order: 6,
            result: 6
        },
    };

    CONFIG.tor1e.WEARY_RESULTS = {
        1: {
            label: `<img src="systems/tor1e/assets/images/dice/s_1_w.png" alt="{{localize tor1e.dice.weary.1}}" />`,
            order: 1,
            result: 0
        },
        2: {
            label: `<img src='systems/tor1e/assets/images/dice/s_2_w.png'  alt="{{localize tor1e.dice.weary.2}}" />`,
            order: 2,
            result: 0
        },
        3: {
            label: `<img src='systems/tor1e/assets/images/dice/s_3_w.png'  alt="{{localize tor1e.dice.weary.3}}" />`,
            order: 3,
            result: 0
        },
        4: {
            label: `<img src='systems/tor1e/assets/images/dice/s_4.png'  alt="{{localize tor1e.dice.weary.4}}" />`,
            order: 4,
            result: 4
        },
        5: {
            label: `<img src='systems/tor1e/assets/images/dice/s_5.png'  alt="{{localize tor1e.dice.weary.5}}" />`,
            order: 5,
            result: 5
        },
        6: {
            label: `<img src='systems/tor1e/assets/images/dice/s_6.png'  alt="{{localize tor1e.dice.weary.6}}" />`,
            order: 6,
            result: 6
        },
    };

    CONFIG.tor1e.FEAT_RESULTS = {
        1: {
            label: `<img src="systems/tor1e/assets/images/dice/f_1.png" alt="{{localize tor1e.dice.feat.1}}" />`,
            adversaryOrder: 2,
            order: 2,
            result: 1
        },
        2: {
            label: `<img src='systems/tor1e/assets/images/dice/f_2.png'  alt="{{localize tor1e.dice.feat.2}}" />`,
            adversaryOrder: 3,
            order: 3,
            result: 2
        },
        3: {
            label: `<img src='systems/tor1e/assets/images/dice/f_3.png'  alt="{{localize tor1e.dice.feat.3}}" />`,
            adversaryOrder: 4,
            order: 4,
            result: 3
        },
        4: {
            label: `<img src='systems/tor1e/assets/images/dice/f_4.png'  alt="{{localize tor1e.dice.feat.4}}" />`,
            adversaryOrder: 5,
            order: 5,
            result: 4
        },
        5: {
            label: `<img src='systems/tor1e/assets/images/dice/f_5.png'  alt="{{localize tor1e.dice.feat.5}}" />`,
            adversaryOrder: 6,
            order: 6,
            result: 5
        },
        6: {
            label: `<img src='systems/tor1e/assets/images/dice/f_6.png'  alt="{{localize tor1e.dice.feat.6}}" />`,
            adversaryOrder: 7,
            order: 7,
            result: 6
        },
        7: {
            label: `<img src="systems/tor1e/assets/images/dice/f_7.png" alt="{{localize tor1e.dice.feat.7}}" />`,
            adversaryOrder: 8,
            order: 8,
            result: 7
        },
        8: {
            label: `<img src='systems/tor1e/assets/images/dice/f_8.png'  alt="{{localize tor1e.dice.feat.8}}" />`,
            adversaryOrder: 9,
            order: 9,
            result: 8
        },
        9: {
            label: `<img src='systems/tor1e/assets/images/dice/f_9.png'  alt="{{localize tor1e.dice.feat.9}}" />`,
            adversaryOrder: 10,
            order: 10,
            result: 9
        },
        10: {
            label: `<img src='systems/tor1e/assets/images/dice/f_10.png'  alt="{{localize tor1e.dice.feat.10}}" />`,
            adversaryOrder: 11,
            order: 11,
            result: 10
        },
        11: {
            label: `<img src='systems/tor1e/assets/images/dice/f_eye.png'  alt="{{localize tor1e.dice.feat.11}}" />`,
            adversaryOrder: 12,
            order: 1,
            result: 0
        },
        12: {
            label: `<img src='systems/tor1e/assets/images/dice/f_gandalf.png'  alt="{{localize tor1e.dice.feat.12}}" />`,
            adversaryOrder: 1,
            order: 12,
            result: 0
        },
    };

    Items.unregisterSheet("core", ItemSheet)
    Items.registerSheet("tor1e", Tor1eItemSheet, {makeDefault: true});

    Actors.unregisterSheet("core", ActorSheet)
    Actors.registerSheet("tor1e", Tor1eCommunitySheet, {types: ["community"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eLoreSheet, {types: ["lore"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eNpcSheet, {types: ["npc"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eAdversarySheet, {types: ["adversary"], makeDefault: true});
    Actors.registerSheet("tor1e", Tor1eCharacterSheet, {types: ["character"], makeDefault: true});

    await preloadHandlebarsTemplates();

    Handlebars.registerHelper('skill-dots', function (n, max, block) {
        let accum = '';
        for (let i = 1; i <= max; ++i)
            if (i <= n) {
                accum += '<div class="skill-display-element-full"></div>';
            } else {
                accum += '<div class="skill-display-element"></div>';
            }
        return accum;
    });

    // Register System Settings
    registerSystemSettings();
})

async function preloadHandlebarsTemplates() {
    const templatePaths = [
        // Common
        "systems/tor1e/templates/sheets/actors/partials/common/faction-slider-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/skill-item-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/weapon-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/simple-talent-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/connexion-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/patron-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/travel-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/member-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/traveller-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/complex-talent-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/common/actor-header-card.hbs",

        // Adversary
        "systems/tor1e/templates/sheets/actors/partials/adversary/special-ability-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/adversary/adversary-skill-card.hbs",

        // Character
        "systems/tor1e/templates/sheets/actors/partials/character/character-biography-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-item-skill-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-attribute-sidebar-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-common-skills-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-skill-groups-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-combat-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-armour-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-weapon-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-state-of-health-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-combat-attributes-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-resources-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/character-richness-card.hbs",
        "systems/tor1e/templates/sheets/actors/partials/character/miscellaneous-item-card.hbs",

        //items

        //roll
        "systems/tor1e/templates/roll/display-formula-card.hbs",

        // Messages
        "systems/tor1e/templates/sheets/messages/partials/common/skill-roll-card.hbs",
        "systems/tor1e/templates/sheets/messages/partials/common/tooltip-tor1e.hbs",

        // Components
        "systems/tor1e/templates/sheets/actors/components/extensions/computed-stat-circle-ext.hbs",
        "systems/tor1e/templates/sheets/actors/components/extensions/stat-circle-ext.hbs",
        "systems/tor1e/templates/sheets/actors/components/extensions/resource-circle-ext.hbs",
        "systems/tor1e/templates/sheets/actors/components/extensions/item-circle-ext.hbs",
        "systems/tor1e/templates/sheets/actors/components/stat-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/resource-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/item-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/computed-stat-circle-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/skill-card.hbs",
        "systems/tor1e/templates/sheets/actors/components/effects-card.hbs",
    ];

    CONFIG.Dice.terms['s'] = TORSuccessDie;
    CONFIG.Dice.terms['w'] = TORWearySuccessDie;
    CONFIG.Dice.terms['f'] = TORFeatBaseDie;
    CONFIG.Dice.terms['e'] = TORSauronicFeatBaseDie;

    return loadTemplates(templatePaths);
}

Hooks.on('renderChatLog', (log, html) => {
    Tor1eRoll.chatListeners(html);
});

Hooks.on('updateCombatant', async (combatant, flags, diff, userId) => {
    let combat = combatant.parent;
    if (!combatant || !combat) return;

    await combat.redrawCombatantToken(combatant.id)
});

Hooks.on('deleteToken', async (scene, token, empty, sceneId) => {
    if (!token || !game.combat) return;
    let combatant = game.combat.getCombatantByToken(token.id);
    if (combatant) {
        await game.combat.deleteEmbeddedDocuments("Combatant", [combatant.id]);
    }
});

/* -------------------------------------------- */

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration
 */
Hooks.once("ready", function () {

    StatusEffects.onReady();

    // Determine whether a system migration is required and feasible
    if (!game.user.isGM) return;

    const migrationScripts = [new Tor1eMigration0_0_22(), new Tor1eMigration0_0_23(), new Tor1eMigration0_0_28()];

    let migrationResult = Tor1eMigration.processMigrationScripts(migrationScripts);

    if (migrationResult.migration) {
        if (migrationResult.result) {
            console.log(`Migration of your World has finished without errors, you can play !`, {permanent: true});
        } else {
            ui.notifications.error(`Migration of your World is a failure, please report to us if you need help !`, {permanent: true});
        }
    } else {
        console.log(`No Data migration was needed for your World !`, {permanent: true});
    }

    activateSocketListener();
});

Hooks.once('diceSoNiceReady', (dice3d) => {
    dice3d.addSystem({id: "tor1e", name: "The One Ring"}, true);

    dice3d.addColorset({
        name: "friendly",
        description: "friendly",
        category: 'Colors',
        foreground: ['#D4AF37'],
        background: ['#ffffff'],
        outline: '222222',
        texture: 'none',
        visible: "hidden"
    });

    dice3d.addColorset({
        name: "hostile",
        description: "hostile",
        category: 'Colors',
        foreground: ['#e3e300'],
        background: ['#000000'],
        outline: 'red',
        texture: 'none',
        visible: "hidden"
    });

    //tor1e success dice
    dice3d.addDicePreset({
        type: "ds",
        labels: ["systems/tor1e/assets/images/dice/s_1.png",
            "systems/tor1e/assets/images/dice/s_2.png",
            "systems/tor1e/assets/images/dice/s_3.png",
            "systems/tor1e/assets/images/dice/s_4.png",
            "systems/tor1e/assets/images/dice/s_5.png",
            "systems/tor1e/assets/images/dice/s_6.png"],
        bumpMaps: [
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_1.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_2.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_3.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_4.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_5.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_6.png",
        ],
        system: "tor1e"
    }, "d6");

    //tor1e weary success dice
    dice3d.addDicePreset({
        type: "dw",
        labels: ["systems/tor1e/assets/images/dice/s_1_w.png",
            "systems/tor1e/assets/images/dice/s_2_w.png",
            "systems/tor1e/assets/images/dice/s_3_w.png",
            "systems/tor1e/assets/images/dice/s_4.png",
            "systems/tor1e/assets/images/dice/s_5.png",
            "systems/tor1e/assets/images/dice/s_6.png"],
        bumpMaps: [
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_1_w.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_2_w.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_3_w.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_4.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_5.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_s_6.png",
        ],
        system: "tor1e"
    }, "d6");

    //tor1e feat dice for friendly character
    dice3d.addDicePreset({
        type: "df",
        labels: ["systems/tor1e/assets/images/dice/f_1.png",
            "systems/tor1e/assets/images/dice/f_2.png",
            "systems/tor1e/assets/images/dice/f_3.png",
            "systems/tor1e/assets/images/dice/f_4.png",
            "systems/tor1e/assets/images/dice/f_5.png",
            "systems/tor1e/assets/images/dice/f_6.png",
            "systems/tor1e/assets/images/dice/f_7.png",
            "systems/tor1e/assets/images/dice/f_8.png",
            "systems/tor1e/assets/images/dice/f_9.png",
            "systems/tor1e/assets/images/dice/f_10.png",
            "systems/tor1e/assets/images/dice/f_eye.png",
            "systems/tor1e/assets/images/dice/f_gandalf.png"],
        bumpMaps: [
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_1.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_2.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_3.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_4.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_5.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_6.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_7.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_8.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_9.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_10.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_eye.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_gandalf.png",
        ],
        colorset: "friendly",
        system: "tor1e"
    }, "d12");

    //tor1e hostile feat dice for hostile character
    dice3d.addDicePreset({
        type: "de",
        labels: ["systems/tor1e/assets/images/dice/sf_1.png",
            "systems/tor1e/assets/images/dice/sf_2.png",
            "systems/tor1e/assets/images/dice/sf_3.png",
            "systems/tor1e/assets/images/dice/sf_4.png",
            "systems/tor1e/assets/images/dice/sf_5.png",
            "systems/tor1e/assets/images/dice/sf_6.png",
            "systems/tor1e/assets/images/dice/sf_7.png",
            "systems/tor1e/assets/images/dice/sf_8.png",
            "systems/tor1e/assets/images/dice/sf_9.png",
            "systems/tor1e/assets/images/dice/sf_10.png",
            "systems/tor1e/assets/images/dice/sf_eye.png",
            "systems/tor1e/assets/images/dice/sf_gandalf.png"],
        bumpMaps: [
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_1.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_2.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_3.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_4.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_5.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_6.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_7.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_8.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_9.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_10.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_eye.png",
            "systems/tor1e/assets/images/dice/bump_maps/bump_f_gandalf.png",
        ],
        colorset: "hostile",
        system: "tor1e"
    }, "d12");

});

registerHooks();

Object.defineProperty(String.prototype, "toBoolean", {
    value: function toBoolean() {
        return this ? this.toLowerCase() === "true" : false;
    },
    writable: true,
    configurable: true
});

Token.prototype.displayStance = async function (imgPath) {

    this.cardTexture = await loadTexture(imgPath, {fallback: CONST.DEFAULT_TOKEN});
    this.card = new PIXI.Sprite(this.cardTexture);
    this.card.anchor.set(-0.5, 0.5);
    this.card.width = this.w * 1.5
    this.card.height = this.h * 3;

    this.card.width = this.w * 1.5
    this.card.height = this.h * 1.5;
    this.card.position.set(this.w / 2, this.h + 2);

    // creating pixi object  from actor.system.cardImage
    this.card.parent = this;

    //size and position
    this.card.anchor.set(-0.5, 0.5);

    //appending card to Token
    this.addChild(this.card);
}

/* -------------------------------------------- */
/* Rendering
/* -------------------------------------------- */
// FIXME: WK
// draw was throwing errors, so temporarily disabled. Change 'xdraw' to 'draw'
// and fix.
Token.prototype.xdraw = async function () {
    this.clear();

    // Clean initial data
    this._cleanData();

    // Draw the token as invisible so it will be safely revealed later
    this.visible = false;

    // Load token texture
    this.texture = await loadTexture(this.img, {fallback: CONST.DEFAULT_TOKEN});

    // Draw Token components
    this.border = this.addChild(new PIXI.Graphics());
    this.icon = this.addChild(await this._drawIcon());

    // Draw the HUD interface
    this._drawHUD();

    // Define initial interactivity and visibility state
    this.hitArea = new PIXI.Rectangle(0, 0, this.w, this.h);
    this.buttonMode = true;

    // Begin video playback
    if (this.isVideo) {
        this.play(true);
        this._unlinkVideoPlayback(this.sourceElement).then(s => this.play(true));
    }

    // Draw the initial position
    this.refresh();
    await this.drawEffects();
    this.drawBars();
    await this.drawStance();

    // Enable interactivity, only if the Tile has a true ID
    if (this.id) this.activateListeners();
    return this;
}

Token.prototype.drawStance = async function () {
    if (!this.scene || tor1eUtilities.combat.noActiveCombatInScene(this.scene.id)) return;
    let combatant = game?.combat?.getActiveCombatants().find(c => c?.token?.id === this._id);
    if (!combatant || !combatant.actor || !combatant.actor.isCharacter) return;

    let stance = combatant.getStance();

    if (!stance) return;

    this.cardTexture = await loadTexture(stance.logo, {fallback: CONST.DEFAULT_TOKEN});
    this.card = PIXI.Sprite.from(this.cardTexture);
    this.card.width = this.w * 0.4;
    this.card.height = this.h * 0.4;
    this.card.position.set(this.w * 0.6 - 1, this.h * 0.6 - 1);

    // Background
    this.card.bg = this.card.bg || this.addChild(new PIXI.Graphics());
    this.card.bg
        .clear()
        .beginFill(0x000000, 0.5)
        .drawRoundedRect(this.w * 0.6 - 2, this.h * 0.6 - 2, this.card.width + 2, this.card.height + 2, 2)
        .endFill();

    // creating pixi object  from actor.system.cardImage
    this.card.parent = this;
    this.card.flags = {stance: true};
    //appending card to Token
    this.addChild(this.card);
}

/**
 * Add or remove the currently controlled Tokens from the active combat encounter
 * @param {Combat} [combat]    A specific combat encounter to which this Token should be added
 * @return {Promise<Token>} The Token which initiated the toggle
 */
Token.prototype.toggleCombat = async function (combat) {
    if (!this.actor || !this.actor || (!this.inCombat && !game.actors.find((a) => a.id === this.actor.id))) {
        ui.notifications.error(game.i18n.format("tor1e.combat.error.noActorAttachedToToken", {
            name: this.name
        }));
        return this;
    }
    await this.layer.toggleCombat(!this.inCombat, combat, {token: this});

    if (this.actor.extendedData.isCharacter) {
        await this.draw();
        this.visible = true;
    }
    return this;
}

/**
 * Toggle Token combat state
 * @private
 */
TokenHUD.prototype._onToggleCombat = async function (event) {
    event.preventDefault();
    await this.object.toggleCombat();
    if (this.object) {
        event.currentTarget.classList.toggle("active", this.object.inCombat);
    }
}

/**
 * Add or remove the set of currently controlled Tokens from the active combat encounter
 * @param {boolean} state         The desired combat state which determines if each Token is added (true) or
 *                                removed (false)
 * @param {Combat|null} combat    A Combat encounter from which to add or remove the Token
 * @param {Token|null} [token]    A specific Token which is the origin of the group toggle request
 * @return {Promise<Combat>}      The updated Combat encounter
 */
TokenLayer.prototype.toggleCombat = async function toggleCombat(state = true, combat = null, {token = null} = {}) {
// Process each controlled token, as well as the reference token
    const tokens = this.controlled.filter(t => t.inCombat !== state);
    if (token && !token.controlled && (token.inCombat !== state)) tokens.push(token);

    // Reference the combat encounter displayed in the Sidebar if none was provided
    combat = combat ?? game.combats.viewed;
    if (!combat) {
        if (game.user.isGM) {
            const cls = getDocumentClass("Combat")
            combat = await cls.create({scene: canvas.scene.id, active: true}, {render: !state || !tokens.length});
        } else return ui.notifications.warn("COMBAT.NoneActive", {localize: true});
    }

    tokens.map(async (t) => {
        if (t.actor.extendedData.isCharacter) {
            await t.draw();
            t.visible = true;
        }
    });

    // Add tokens to the Combat encounter
    if (state) {
        const createData = tokens.map(t => {
            return {tokenId: t.id, actorId: t.document.actorId, hidden: t.document.hidden}
        });
        return combat.createEmbeddedDocuments("Combatant", createData);
    }

    // Remove Tokens from combat
    if (!game.user.isGM) return combat;
    const tokenIds = new Set(tokens.map(t => t.id));
    const combatantIds = combat.combatants.reduce((ids, c) => {
        let tokenId = c.tokenId;
        if (tokenIds.has(tokenId)) {
            let token = canvas.tokens.get(tokenId);
            ids.push(c.id);
            if (token?.actor?.extendedData?.isCharacter && typeof c?.setStance === "function") {
                c?.setStance(null);
                token.draw();
                token.visible = true;
            }
        }
        return ids;
    }, []);
    return combat.deleteEmbeddedDocuments("Combatant", combatantIds);
}