import {
    Tor1eDefensiveStance,
    Tor1eForwardStance,
    Tor1eOpenStance,
    Tor1eRearwardStance,
    Tor1eStance
} from "../combat/Tor1eStance.js";

export class Tor1eTokenHudExtension {

    static default() {
        // Integration du TokenHUD
        Hooks.on('renderTokenHUD', (tokenHud, html, token) => {
            Tor1eTokenHudExtension.manage(tokenHud, html, token._id)
        });
    }

    static manage(tokenHud, html, tokenId) {
        let token = canvas.tokens.get(tokenId);
        let actor = token.actor;

        let combat = game.combat;

        if (!combat) return;

        function _getCombatant() {
            return combat.getActiveCombatants().find(combatant =>
                combatant?.token?.id === token?.id);
        }

        function _isNotInCombat() {
            return _getCombatant() === undefined;
        }

        let step = combat.getCombatStep();
        if (!step
            || !step.currentStepIsCloseCombat
            || _isNotInCombat()
            || actor === undefined
            || !actor.extendedData.isCharacter) {
            return;
        }

        Tor1eTokenHudExtension._add(html, token, combat, _getCombatant(), tokenHud);
    }

    /* -------------------------------------------- */
    static async _add(html, token, combat, combatant, tokenHud) {

        function _buildStanceObject(stance, combatant) {
            let combatData = combatant.getCombatData();
            let combatantStance = combatData.stance;
            let jsonStance = stance.toJSON();
            return {
                stance: jsonStance,
                isActive: (combatantStance && combatantStance.class === jsonStance.class) || false
            }
        }

        let template = "systems/tor1e/templates/hud/combat-stances.hbs";
        let stances = {
            stances: [
                _buildStanceObject(new Tor1eForwardStance(), combatant),
                _buildStanceObject(new Tor1eOpenStance(), combatant),
                _buildStanceObject(new Tor1eDefensiveStance(), combatant),
                _buildStanceObject(new Tor1eRearwardStance(), combatant),
            ]
        };
        // Create space for Hud Extensions next to elevation icon
        let divTokenHudExt = '<div class="tokenhudext left">';
        html.find('.attribute.elevation').wrap(divTokenHudExt);

        const stancesHud = $(await renderTemplate(template, stances));

        html.find('.attribute.elevation').before(stancesHud);// Add Movement token tip

        html.find('.control-icon.combat-stance').click(async event => {
            event.preventDefault();
            event.stopPropagation();
            let element = event.currentTarget;
            let stanceClass = element.dataset.stanceType;
            let combatant = combat.combatants.find(c => c.token.id === token.id);
            let combatData = combatant.getCombatData();
            if (combatData.stance.class === stanceClass) {
                return;
            }

            let newStance = Tor1eStance.from(stanceClass).toJSON();
            let opponents = combatData.engagedWith || [];
            opponents.map(async foe => {
                    //update the stance of the foe because the character stance has changed.
                    let foeCombatant = combat.combatants.find(c => c.token.id === foe.tokenId);

                    await this.updateCombatantStance(foeCombatant, newStance, tokenHud);
                }
            );

            await this.updateCombatantStance(combatant, newStance, tokenHud);

        });
    }

    static async updateCombatantStance(combatant, stance, tokenHud) {
        let stanceData = {
            stance: stance
        };
        let updateData = {
            _id: combatant._id,
            combatId : game.combat.id,
            flags: stanceData
        };
        if (!game.user.isGM) {
            game.socket.emit("system.tor1e", {
                type: "updateCombatantStance",
                payload: updateData
            })
        } else {
            let combatData = combatant.getCombatData();
            combatData.stance = stance;
            await combatant.setCombatData(combatData);
            // Need to render the Hud since we made some change
            await combatant.token.render();
        }
    }
}