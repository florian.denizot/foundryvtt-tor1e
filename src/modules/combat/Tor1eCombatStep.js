export class Tor1eCombatStep {


    getTitle () {
        return "tor1e.combat.step.default.label";
    }

    /**
     * Represent the data of the Roll as an object suitable for JSON serialization.
     * @return {Object}     Structured data which can be serialized into JSON
     */
    toJSON() {
        return {
            class: this.constructor.name,
            title: this.getTitle(),
            nextStepIsInitiative: this.nextStepIsInitiative(),
            nextStepIsOpeningVolley: this.nextStepIsOpeningVolley(),
            nextStepIsCloseCombat: this.nextStepIsCloseCombat(),
            currentStepIsInitiative: this.currentStepIsInitiative(),
            currentStepIsOpeningVolley: this.currentStepIsOpeningVolley(),
            currentStepIsCloseCombat: this.currentStepIsCloseCombat()
        };
    }

    currentStepIsInitiative() {
        return false;
    }

    currentStepIsOpeningVolley() {
        return false;
    }

    currentStepIsCloseCombat() {
        return false;
    }

    nextStepIsInitiative() {
        return false;
    }

    nextStepIsOpeningVolley() {
        return false;
    }

    nextStepIsCloseCombat() {
        return false;
    }

    static fromData(data) {
        if (data.class === "Tor1eInitiativeStep") {
            return new Tor1eInitiativeStep();
        }
        if (data.class === "Tor1eOpeningVolleyStep") {
            return new Tor1eOpeningVolleyStep();
        }
        if (data.class === "Tor1eCloseCombatStep") {
            return new Tor1eCloseCombatStep();
        }
    }

}

export class Tor1eInitiativeStep extends Tor1eCombatStep {

    /** @override */
    currentStepIsInitiative() {
        return true;
    }

    /** @override */
    nextStepIsOpeningVolley() {
        return true;
    }

    /** @override */
    getTitle () {
        return "tor1e.combat.step.preparation.label";
    }
}

export class Tor1eOpeningVolleyStep extends Tor1eCombatStep {

    /** @override */
    currentStepIsOpeningVolley() {
        return true;
    }

    /** @override */
    nextStepIsCloseCombat() {
        return true;
    }

    /** @override */
    getTitle () {
        return "tor1e.combat.step.openingVolley.label";
    }
}

export class Tor1eCloseCombatStep extends Tor1eCombatStep {

    /** @override */
    currentStepIsCloseCombat() {
        return true;
    }

    /** @override */
    getTitle () {
        return "tor1e.combat.step.closeCombat.label";
    }
}

