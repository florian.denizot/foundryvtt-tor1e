export class Tor1eStance {

    getTitle() {
        return "tor1e.combat.stance.default.label";
    }

    getLogo() {
        return "icons/svg/mystery-man.svg";
    }

    getDifficulty() {
        return 0;
    }

    getBaseOrderValue() {
        return 0;
    }

    /**
     * Represent the data of the Roll as an object suitable for JSON serialization.
     * @return {Object}     Structured data which can be serialized into JSON
     */
    toJSON() {
        return {
            class: this.constructor.name,
            title: this.getTitle(),
            difficulty: this.getDifficulty(),
            baseOrderValue: this.getBaseOrderValue(),
            logo: this.getLogo(),
        };
    }

    static from(clazz) {
        if (clazz === "Tor1eForwardStance") {
            return new Tor1eForwardStance();
        }
        if (clazz === "Tor1eOpenStance") {
            return new Tor1eOpenStance();
        }
        if (clazz === "Tor1eDefensiveStance") {
            return new Tor1eDefensiveStance();
        }
        if (clazz === "Tor1eRearwardStance") {
            return new Tor1eRearwardStance();
        }
    }

}

export class Tor1eForwardStance extends Tor1eStance {

    /** @override */
    getTitle() {
        return "tor1e.combat.stance.forward.label";
    }

    /** @override */
    getDifficulty() {
        return 6;
    }

    getBaseOrderValue() {
        return 400;
    }

    /** @override */
    getLogo() {
        return "/systems/tor1e/assets/images/hud/forward-stance.svg";
    }
}

export class Tor1eOpenStance extends Tor1eStance {

    /** @override */
    getTitle() {
        return "tor1e.combat.stance.open.label";
    }

    /** @override */
    getDifficulty() {
        return 9;
    }

    getBaseOrderValue() {
        return 300;
    }

    /** @override */
    getLogo() {
        return "/systems/tor1e/assets/images/hud/open-stance.svg";
    }
}

export class Tor1eDefensiveStance extends Tor1eStance {

    /** @override */
    getTitle() {
        return "tor1e.combat.stance.defensive.label";
    }

    /** @override */
    getDifficulty() {
        return 12;
    }

    getBaseOrderValue() {
        return 200;
    }

    /** @override */
    getLogo() {
        return "/systems/tor1e/assets/images/hud/defensive-stance.svg";
    }
}

export class Tor1eRearwardStance extends Tor1eStance {

    /** @override */
    getTitle() {
        return "tor1e.combat.stance.rearward.label";
    }

    /** @override */
    getDifficulty() {
        return 12;
    }

    getBaseOrderValue() {
        return 100;
    }

    /** @override */
    getLogo() {
        return "/systems/tor1e/assets/images/hud/rearward-stance.svg";
    }
}

