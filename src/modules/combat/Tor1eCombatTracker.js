import {tor1eUtilities} from "../utilities.js";
import {Tor1eRoll} from "../Tor1eRoll.js";
import Tor1eCombatantConfig from "./Tor1eCombatantConfig.js";
import Tor1eChatMessage from "../chat/Tor1eChatMessage.js";

export default class Tor1eCombatTracker extends CombatTracker {

    /** @override */
    get template() {
        return "systems/tor1e/templates/combat/combat-tracker.hbs";
    }

    async getData(options) {
        const data = await super.getData(options);

        if (!data.hasCombat) {
            return data;
        }

        if (game.user.isGM) {
            data.combat.combatants.map(combatant => combatant.setInitiative(data.combat));
        }

        for (let [i, combatant] of data.combat.turns.entries()) {
            let poolDice = combatant.getPoolDice();

            if (combatant?.token && combatant?.actor) {
                data.turns[i] = mergeObject({
                    combatant: combatant,
                    tokenId: combatant.token.id,
                    actorId: combatant.actor.id,
                    hasRolledPrepRoll: parseInt(poolDice) >= 0,
                    poolDice: poolDice,
                    isCharacter: combatant.actor.extendedData.isCharacter,
                    combat: combatant.getCombatData(),
                }, data.turns[i]);
            }

        }
        return data;
    }

    /**
     * Get the sidebar directory entry context options
     * @return {Object}   The sidebar entry context options
     * @private
     * @override
     */
    _getEntryContextOptions() {
        return [
            {
                name: "COMBAT.CombatantUpdate",
                icon: '<i class="fas fa-edit"></i>',
                callback: this._onConfigureCombatant.bind(this)
            },
            {
                name: "tor1e.combat.step.preparation.roll.reroll",
                icon: '<i class="fas fa-dice"></i>',
                callback: li => {
                    let combatantId = li.data('combatant-id');
                    let combatant = this.viewed.combatants.get(combatantId);
                    if (!combatant.actor.extendedData.isCharacter) {
                        ui.notifications.error(
                            game.i18n.format("tor1e.combat.error.unableToRerollPrepBattle",
                                {
                                    token: combatant.token.name,
                                    type: combatant.actor.type
                                }));
                        return;
                    }
                    return this._rollCombatPreparation(combatantId);
                },
                condition: li => this._itemIsACharacter(li)
            },
            {
                name: "tor1e.combat.step.preparation.roll.increase",
                icon: '<i class="fa-solid fa-plus"></i>',
                callback: li => {
                    let combatantId = li.data('combatant-id');
                    let combatant = this.viewed.combatants.get(combatantId);
                    if (!combatant.actor.extendedData.isCharacter) {
                        ui.notifications.error(
                            game.i18n.format("tor1e.combat.error.unableToRerollPrepBattle",
                                {
                                    token: combatant.token.name,
                                    type: combatant.actor.type
                                }));
                        return;
                    }
                    return this._increaseCombatantPoolDice(combatantId);
                },
                condition: li => this._itemIsACharacter(li)
            },
            {
                name: "tor1e.combat.step.preparation.roll.decrease",
                icon: '<i class="fa-solid fa-minus"></i>',
                callback: li => {
                    let combatantId = li.data('combatant-id');
                    let combatant = this.viewed.combatants.get(combatantId);
                    if (!combatant.actor.extendedData.isCharacter) {
                        ui.notifications.error(
                            game.i18n.format("tor1e.combat.error.unableToRerollPrepBattle",
                                {
                                    token: combatant.token.name,
                                    type: combatant.actor.type
                                }));
                        return;
                    }
                    return this._decreaseCombatantPoolDice(combatantId);
                },
                condition: li => this._itemIsACharacter(li)
            },
            {
                name: "COMBAT.CombatantRemove",
                icon: '<i class="fas fa-skull"></i>',
                callback: li => this.viewed.deleteCombatant(li.data('combatant-id'))
            },
        ];
    }

    _itemIsACharacter(li) {
        let combatantId = li.data('combatant-id');
        let combatant = this.viewed.combatants.get(combatantId);
        return combatant.actor.extendedData.isCharacter
    }

    /* -------------------------------------------- */

    /**
     * Display a dialog which prompts the user to enter a new initiative value for a Combatant
     * @param {jQuery} li
     * @private
     * @override
     */
    _onConfigureCombatant(li) {
        const combatant = this.viewed.combatants.get(li.data('combatant-id'));
        new Tor1eCombatantConfig(combatant, {
            top: Math.min(li[0].offsetTop, window.innerHeight - 350),
            left: window.innerWidth - 720,
            width: 400
        }).render(true);
    }

    /**
     * Handle mouse-hover events on a combatant in the tracker
     * @private
     */
    _onCombatantHoverIn(event) {
        event.preventDefault();
        if ( !canvas.ready ) return;
        const li = event.currentTarget;
        const combatant = this.viewed.combatants.get(li.dataset.combatantId);
        const token = combatant?.token?.object;
        if ( token?.isVisible ) {
            if ( !token.controlled ) token._onHoverIn(event);
            this._highlighted = token;
        }
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find(".att-def").click(ev => this._onAttDefClick(ev));
        html.find(".roll-battle").click(ev => this._listenerOnBattleRoll(ev));

        // Combat control
        html.find('.combat-control').click(ev => this._onCombatControl(ev));
    }

    async _rollCombatPreparation(combatantId) {
        let skillName = "battle";
        let actionName = game.i18n.localize("tor1e.combat.step.preparation.roll.label");
        let automaticDifficultyRoll = true;
        let associateRawAttribute = "heart";
        let combatant = this.viewed.combatants.get(combatantId);
        let actionFavouredValue = combatant.actor.system.attributes.heart.favoured.value;
        let combatData = combatant.getCombatData();
        const token = combatant.token;
        let difficulty = combatData.attackers ? 18 : 14;
        const optionData = {
            displayRoll: true,
            actorId: combatant.actor.id,
            actorName: combatant.actor.name,
            token: {
                id: token ? token.id : "",
                name: token ? token.name : "",
            },
            difficulty: difficulty,
            combatId: this.viewed.id
        }
        const hopePointPostAction = {
            fn: "createBattleRollResultChatMessage",
            options: optionData,
        }
        const extra = {
            hopePointPostAction: hopePointPostAction,
            token: token,
            combatId: this.viewed.id
        }
        return await tor1eUtilities.rolling.castCombatPreparationRoll(token, skillName, actionName, automaticDifficultyRoll, associateRawAttribute, actionFavouredValue, difficulty, extra)

    }

    async _listenerOnBattleRoll(event) {
        event.preventDefault();
        event.stopPropagation();
        let element = event.currentTarget;
        let tokenId = element.closest(".combatant").dataset.tokenId;
        let rollResult = await this.listenerRollASkill(event, tokenId);
        if (rollResult) {
            await this._updateCombatantPoolDice(rollResult, tokenId);
        }
    }

    async _onAttDefClick(event) {
        event.preventDefault();
        event.stopPropagation();
        let currentCombat = this.viewed;
        await currentCombat.togglePcsAreAttacking()
    }

    async listenerRollASkill(event, tokenId) {
        event.preventDefault();
        let element = event.currentTarget;
        let associateRawAttribute = element.dataset.associateAttributeName;
        let actionFavouredValue = element.dataset.actionFavouredValue;
        let automaticDifficultyRoll = !event.shiftKey;
        let skillName = element.dataset.rolledSkill;
        let actionName = element.dataset.rolledSkillLabel;
        const combatant = this.viewed.getCombatantByToken(tokenId);
        let combatData = Tor1eChatMessage.getExtendedData(this.viewed);
        const token = combatant.token;
        let difficulty = combatData.attackers ? 18 : 14;
        const optionData = {
            displayRoll: true,
            actorId: combatant.actor.id,
            token: {
                id: token ? token.id : "",
                name: token ? token.name : "",
            },
            difficulty: difficulty,
            combatId: this.viewed.id
        }
        const hopePointPostAction = {
            fn: "createBattleRollResultChatMessage",
            options: optionData,
        }
        const extra = {
            hopePointPostAction: hopePointPostAction,
            token: token,
            combatId: this.viewed.id
        }
        return await tor1eUtilities.rolling.castCombatPreparationRoll(token, skillName, actionName, automaticDifficultyRoll, associateRawAttribute, actionFavouredValue, difficulty, extra)
    }

    async _increaseCombatantPoolDice(combatantId) {
        let combatant = this.viewed.combatants.get(combatantId);
        let poolDice = (combatant?.flags?.tor1e?.poolDice ?? 0) + 1;
        let updateData = {
            _id: combatant._id,
            uuid: combatant.uuid,
            combatId: game.combat.id,
            flags: poolDice
        };
        if (!game.user.isGM) {
            game.socket.emit("system.tor1e", {
                type: "updateCombatantPoolDice",
                payload: updateData
            })
        } else {
            await combatant.setPoolDice(poolDice);
        }
    }

    async _decreaseCombatantPoolDice(combatantId) {
        let combatant = this.viewed.combatants.get(combatantId);
        let newPoolDice = (combatant?.flags?.tor1e?.poolDice ?? 1) - 1;
        let poolDice = newPoolDice < 0 ? 0 : newPoolDice;
        let updateData = {
            _id: combatant._id,
            uuid: combatant.uuid,
            combatId: game.combat.id,
            flags: poolDice
        };
        if (!game.user.isGM) {
            game.socket.emit("system.tor1e", {
                type: "updateCombatantPoolDice",
                payload: updateData
            })
        } else {
            await combatant.setPoolDice(poolDice);
        }
    }

    async _updateCombatantPoolDice(rollResult, tokenId) {
        let rawRoll = rollResult.toJSON();
        let roll = await Tor1eRoll.fromData(rawRoll);
        let combatant = this.viewed.getCombatantByToken(tokenId);
        let poolDice = roll.customResult.result.type.nbBonusDice;
        let updateData = {
            _id: combatant._id,
            uuid: combatant.uuid,
            combatId: game.combat.id,
            flags: poolDice
        };
        if (!game.user.isGM) {
            game.socket.emit("system.tor1e", {
                type: "updateCombatantPoolDice",
                payload: updateData
            })
        } else {
            await combatant.setPoolDice(poolDice);
        }
    }

}