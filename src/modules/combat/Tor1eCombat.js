import {Tor1eCloseCombatStep, Tor1eInitiativeStep, Tor1eOpeningVolleyStep} from "./Tor1eCombatStep.js";
import {Tor1eStance} from "./Tor1eStance.js";
import {Tor1eRoll} from "../Tor1eRoll.js";

export default class Tor1eCombat extends Combat {

    async _preCreate(data, options, userId) {
        await super._preCreate(data, options, userId);
        this.updateSource({
            "flags.tor1e": {
                attackers: false,
                step: new Tor1eInitiativeStep().toJSON(),
            }
        });
    }

    prepareData() {
        super.prepareData();
        this.step = this.getCombatStep();
    }

    getPcsAreAttacking() {
        return this.getFlag("tor1e", "attackers");
    }

    async togglePcsAreAttacking() {
        await this.setFlag("tor1e", "attackers", !this.getPcsAreAttacking());
    }

    getCombatStep() {
        return this.getFlag("tor1e", "step");
    }

    async setCombatStep(step) {
        await this.setFlag("tor1e", "step", step.toJSON());
    }

    getActiveCombatants() {
        return this.combatants.filter((c) => c.actor !== null || c.token !== null);
    }

    async openingVolley(combat) {
        return await this.setCombatStep(new Tor1eOpeningVolleyStep());
    }

    async startCombat(combat) {
        await this.setCombatStep(new Tor1eCloseCombatStep());
        return super.startCombat(combat);
    }

    async engagementAction(combat) {
        let selectedTokens = canvas.tokens.controlled;
        if (!selectedTokens || selectedTokens.length === 0) {
            ui.notifications.warn(game.i18n.localize("tor1e.combat.warn.noTokenSelectedForEngagement"));
            return;
        }

        //split the list of tokens into 2 lists, one for lightServants and one for shadowServants.
        let shadowServants = selectedTokens.filter(token => token.actor.extendedData.isHostile);
        let lightServants = selectedTokens.filter(token => token.actor.extendedData.isFriendly);

        if (shadowServants.length === 0 || lightServants.length === 0) {
            ui.notifications.warn(game.i18n.localize("tor1e.combat.warn.impossibleForMonoFaction"));
            return;
        }

        if (shadowServants.length !== 1 && lightServants.length !== 1) {
            ui.notifications.warn(game.i18n.localize("tor1e.combat.warn.impossibleForComplexEngagement"));
            return;
        }

        async function _assignFoeTo(token, combat, opponents, manageStance = false) {
            let combatant = combat.getActiveCombatants().find(combatant =>
                combatant.token.id === token.id);

            let foes = opponents.map(function (foe) {
                let foeCombatant = combat.getActiveCombatants().find(combatant =>
                    combatant.token.id === foe.id);
                return {
                    tokenId: foe.id,
                    name: foe.actor.name,
                    img: foe.actor.img,
                    stanceTn: foeCombatant.getStance().difficulty,
                    stanceClass: foeCombatant.getStance().class
                };
            });


            let updateData

            if (manageStance) {
                let fastestFoe = foes.reduce(function (res, obj) {
                    return (obj.stanceTn < res.stanceTn) ? obj : res;
                });
                let stance = Tor1eStance.from(fastestFoe.stanceClass);

                updateData = {
                    inheritedStance: true,
                    engagedWith: foes,
                    stance: stance.toJSON()
                };
            } else {
                updateData = {
                    inheritedStance: false,
                    engagedWith: foes
                };
            }

            await combatant.setCombatData(updateData);
        }

        lightServants.map(token => _assignFoeTo(token, this, shadowServants));
        shadowServants.map(token => _assignFoeTo(token, this, lightServants, true));
    }

    /* -------------------------------------------- */

    /**
     * Display a dialog querying the GM whether they wish to end the combat encounter and empty the tracker
     * @override
     * @return {Promise<void>}
     */
    async endCombat() {
        return Dialog.confirm({
            title: "End Combat Encounter?",
            content: "<p>End this combat encounter and empty the turn tracker?</p>",
            yes: async () => {
                if (this.hasCombatants()) {
                    await Promise.all(
                        this.getActiveCombatants()
                            .filter((c) => c.actor.extendedData.isCharacter)
                            .map(async (c) => {
                                await this.deleteCombatant(c.id);
                            })
                    );
                }
                this.delete();
            }
        });
    }

    hasCombatants() {
        return this.getActiveCombatants().length >= 1;
    }

    /* -------------------------------------------- */

    /**
     * Update an existing Combatant embedded entity
     * @override
     * @see {@link Combat#updateEmbeddedDocument}
     */
    async updateCombatant(data, options) {
        let updateResult = await this.updateEmbeddedDocuments("Combatant", [data], options);
        await this.redrawCombatantToken(data.id);

        return updateResult;
    }

    /**
     * Update an existing Combatant embedded entity
     * @override
     * @see {@link Combat#updateEmbeddedDocument}
     */
    async updateCombatant(data, options = {}) {
        const combatantId = data._id;

        const combatant = this.getActiveCombatants().find(c => c.id === combatantId);
        if (!combatant) return

        await combatant.updateStance(data.flags.stance);
    }

    async redrawCombatantToken(combatantId) {
        let combatant = this.getActiveCombatants().find((c) => c.id === combatantId);
        if (combatant && combatant.actor && combatant.actor.extendedData.isCharacter) {
            /* draw all tokens in combat to refresh the stance icon */
            let combatantToken = combatant?.token;
            if (combatantToken) {
                let token = canvas.tokens.get(combatantToken?.id)
                await token.draw();
                token.visible = true;
            }
        }
    }

    /* -------------------------------------------- */

    /**
     * Delete an existing Combatant embedded entity
     * @override
     * @see {@link Combat#deleteEmbeddedDocuments}
     */
    async deleteCombatant(id, options) {
        let combatant = this.getActiveCombatants().find((c) => c.id === id);
        if (!combatant) return;
        let deleteResult = await this.deleteEmbeddedDocuments("Combatant", [combatant.id], options);
        let combatantToken = combatant?.token;
        if (combatantToken) {
            let token = canvas.tokens.get(combatantToken.id);
            if (token) {
                await token.draw();
                token.visible = true;
            }
        }
        return deleteResult;
    }

    async _updateCombatantPoolDice(rollResult, tokenId) {
        let rawRoll = rollResult.toJSON();
        let roll = await Tor1eRoll.fromData(rawRoll);
        let combatant = this.getCombatantByToken(tokenId);
        let poolDice = roll.customResult.result.type.nbBonusDice;
        let updateData = {
            _id: combatant._id,
            uuid: combatant.uuid,
            combatId: game.combat.id,
            flags: poolDice
        };
        if (!game.user.isGM) {
            game.socket.emit("system.tor1e", {
                type: "updateCombatantPoolDice",
                payload: updateData
            })
        } else {
            await combatant.setPoolDice(poolDice);
        }
    }

}