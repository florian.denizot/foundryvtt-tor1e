export default class Tor1eCombatantConfig extends CombatantConfig {

    /** @override */
    get template() {
        return "systems/tor1e/templates/combat/combatant-config.hbs";
    }

    getData(options) {
        let data = super.getData(options);
        data.object = data.document ;
        data.object.hasPoolDice = data.document.actor.extendedData.isCharacter ;
        data.object.poolDice = data.document.flags.tor1e.poolDice;
        return data;
    }

}