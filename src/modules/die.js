export class TORBaseDie extends Die {

    /**
     * @override
     *
     * A helper method to modify the results array of a dice term by flagging certain results are kept or dropped.
     * @param {object[]} results      The results array
     * @param {number} number         The number to keep or drop
     * @param {boolean} [keep]        Keep results?
     * @param {boolean} [highest]     Keep the highest?
     * @return {object[]}             The modified results array
     */
    static _keepOrDrop(results, number, {keep = true, highest = true} = {}) {

        // Sort remaining active results in ascending (keep) or descending (drop) order
        const ascending = keep === highest;
        const values = results.reduce((arr, r) => {
            if (r.active) arr.push(r.customResult);
            return arr;
        }, []).sort((a, b) => ascending ? a - b : b - a);

        // Determine the cut point, beyond which to discard
        number = Math.clamped(keep ? values.length - number : number, 0, values.length);
        const cut = values[number];

        // Track progress
        let discarded = 0;
        const ties = [];
        let comp = ascending ? "<" : ">";

        // First mark results on the wrong side of the cut as discarded
        results.forEach(r => {
            if (!r.active) return;  // Skip results which have already been discarded
            let discard = this.compareResult(r.customResult, comp, cut);
            if (discard) {
                r.discarded = true;
                r.active = false;
                discarded++;
            } else if (r.customResult === cut) ties.push(r);
        });

        // Next discard ties until we have reached the target
        ties.forEach(r => {
            if (discarded < number) {
                r.discarded = true;
                r.active = false;
                discarded++;
            }
        });
        return results;
    }

}

export class TORSuccessDie extends TORBaseDie {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 's';

    static COMMAND = `d${TORSuccessDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_s_6.png"

    static dieLabel = "Success";

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.STANDARD_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        return data.actionValue && index < data.actionValue ? "mandatory" : "optional"
    }

    static customSort(current, next) {
        return next.result - current.result;
    }

    /** @override */
    get total() {
        return super.total;
    }


    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.masteryDiceAdded > 0) {
            return results
                .sort((a, b) => CONFIG.tor1e.STANDARD_RESULTS[b.result].order - CONFIG.tor1e.STANDARD_RESULTS[a.result].order)
                .slice(0, data.actionValue)
                .map(die =>
                    CONFIG.tor1e.STANDARD_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        } else {
            return results.map(die =>
                CONFIG.tor1e.STANDARD_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        }
    }
}

export class TORWearySuccessDie extends TORBaseDie {
    constructor(termData) {
        termData.faces = 6;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'w';

    static COMMAND = `d${TORWearySuccessDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_s_3_w.png"

    static dieLabel = "Weary";

    /**
     * Roll the DiceTerm by mapping a random uniform draw against the faces of the dice term.
     * @param {boolean} [minimize]    Apply the minimum possible result instead of a random result.
     * @param {boolean} [maximize]    Apply the maximum possible result instead of a random result.
     * @return {object}
     */
    roll({minimize = false, maximize = false} = {}) {
        const rand = CONFIG.Dice.randomUniform();
        let result = Math.ceil(rand * this.faces);
        if (minimize) result = 0;
        if (maximize) result = 6;
        let sortResult = result >= 4 ? result : 0;
        let customResult = result >= 4 ? result : 0;
        const roll = {
            result: result,
            active: true,
            sortResult: sortResult,
            customResult: customResult,
        };
        this.results.push(roll);
        return roll;
    }

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.WEARY_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        return data.actionValue && index < data.actionValue ? "mandatory" : "optional"
    }

    /**
     * @override
     * Return the total result of the DicePool if it has been evaluated
     * @type {number|null}
     */
    get total() {
        if (!this._evaluated) return null;
        return this.results.reduce((t, r) => {
            if (!r.active || r.result <= 3) return t;
            if (r.count !== undefined) return t + r.count;
            else return t + r.result;
        }, 0);
    }

    static customSort(current, next) {
        let customThisResult = current.active ? current.result : 0;
        let customNextResult = next.active ? next.result : 0;
        return customNextResult - customThisResult;
    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.masteryDiceAdded > 0) {
            return results
                .sort((a, b) => CONFIG.tor1e.WEARY_RESULTS[b.result].order - CONFIG.tor1e.WEARY_RESULTS[a.result].order)
                .slice(0, data.actionValue)
                .map(die =>
                    CONFIG.tor1e.WEARY_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        } else {
            return results.map(die =>
                CONFIG.tor1e.WEARY_RESULTS[die.result].result).reduce((a, b) => a + b, 0);
        }
    }
}

export class TORFeatBaseDie extends TORBaseDie {
    constructor(termData) {
        termData.faces = 12;
        super(termData);
    }

    /* -------------------------------------------- */

    /**
     * @override
     * Roll the DiceTerm by mapping a random uniform draw against the faces of the dice term.
     * @return {object}
     *  - minimize    Apply the minimum possible result instead of a random result.
     *  - maximize    Apply the maximum possible result instead of a random result.
     * @return {object}
     */
    roll({minimize = false, maximize = false} = {}) {
        const rand = CONFIG.Dice.randomUniform();
        let result = Math.ceil(rand * this.faces);
        if (minimize) result = 0;
        if (maximize) result = 12;
        let sortResult = result !== 11 ? result : 0;
        let customResult = result !== 11 ? result : 0;
        const roll = {
            result: result,
            active: true,
            sortResult: sortResult,
            customResult: customResult,
        };
        this.results.push(roll);
        return roll;
    }

    /** @override */
    static DENOMINATION = 'f';

    static COMMAND = `d${TORFeatBaseDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_f_gandalf.png"

    static dieLabel = "Feat";

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.FEAT_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        if (data.bestFeatDie) {
            return index === 0 ? "mandatory" : "optional";
        } else {
            return index === 0 ? "optional" : "mandatory";
        }
    }

    /**
     * @override
     * Return the total result of the DicePool if it has been evaluated
     * @type {number|null}
     */
    get total() {
        if (!this._evaluated) return null;
        return this.results.reduce((t, r) => {
            if (!r.active || r.result >= 11) return t;
            if (r.count !== undefined) return t + r.count;
            else return t + r.result;
        }, 0);
    }

    static customSort(current, next, data) {
        if (data.bestFeatDie) {
            return next.sortResult - current.sortResult;
        } else {
            return -(next.sortResult - current.sortResult);
        }
    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.bestFeatDie) {
            return Math.max.apply(Math, results.map(function (o) {
                return o.customResult;
            }));
        } else {
            return Math.min.apply(Math, results.map(function (o) {
                return o.customResult;
            }));
        }
    }

    /**
     * @override
     *
     * Keep a certain number of highest or lowest dice rolls from the result set.
     *
     * 20d20k       Keep the 1 highest die
     * 20d20kh      Keep the 1 highest die
     * 20d20kh10    Keep the 10 highest die
     * 20d20kl      Keep the 1 lowest die
     * 20d20kl10    Keep the 10 lowest die
     *
     * @param {string} modifier     The matched modifier query
     */
    keep(modifier) {
        const rgx = /[kK]([hHlL])?([0-9]+)?/;
        const match = modifier.match(rgx);
        if (!match) return this;
        let [direction, number] = match.slice(1);
        direction = direction ? direction.toLowerCase() : "h";
        number = parseInt(number) || 1;
        TORBaseDie._keepOrDrop(this.results, number, {keep: true, highest: direction === "h"});
    }

}

export class TORSauronicFeatBaseDie extends Die {
    constructor(termData) {
        termData.faces = 12;
        super(termData);
    }

    /* -------------------------------------------- */

    /** @override */
    static DENOMINATION = 'e';

    static COMMAND = `d${TORSauronicFeatBaseDie.DENOMINATION}`;

    static IMG = "systems/tor1e/assets/images/chat/dice_icons/chat_f_eye.png"

    static dieLabel = "Feat";

    /* -------------------------------------------- */
    /** @override */
    static getResultLabel(result) {
        return CONFIG.tor1e.FEAT_RESULTS[result].label;
    }

    static getCssClassName(index, data) {
        if (data.bestFeatDie) {
            return index <= 0 ? "mandatory" : "optional"
        } else {
            return index <= 0 ? "optional" : "mandatory"
        }
    }

    /**
     * Roll the DiceTerm by mapping a random uniform draw against the faces of the dice term.
     * @param {boolean} [minimize]    Apply the minimum possible result instead of a random result.
     * @param {boolean} [maximize]    Apply the maximum possible result instead of a random result.
     * @return {object}
     */
    roll({minimize = false, maximize = false} = {}) {
        const rand = CONFIG.Dice.randomUniform();
        let result = Math.ceil(rand * this.faces);
        if (minimize) result = 0;
        if (maximize) result = 11;
        let sortResult = result !== 12 ? result : 0;
        let customResult = result !== 12 ? result : 0;
        const roll = {
            result: result,
            active: true,
            sortResult: sortResult,
            customResult: customResult,
        };
        this.results.push(roll);
        return roll;
    }

    /**
     * @override
     * Return the total result of the DicePool if it has been evaluated
     * @type {number|null}
     */
    get total() {
        if (!this._evaluated) return null;
        return this.results.reduce((t, r) => {
            if (!r.active || r.result >= 11) return t;
            if (r.count !== undefined) return t + r.count;
            else return t + r.result;
        }, 0);
    }

    static customSort(current, next, data) {
        let customThisResult = !current.active && current.result === 12 ? 0 : current.result;
        let customNextResult = next.active ? next.result : 0;
        if (data.bestFeatDie) {
            return customNextResult - customThisResult;
        } else {
            return customThisResult - customNextResult;
        }

    }

    /**
     *
     * @param results
     * @param data some data available as a context for the evaluation
     * @returns {*} the value of the die
     */
    static getResultValue(results, data) {
        if (data.bestFeatDie) {
            return Math.max.apply(Math, results.map(function (o) {
                return o.customResult;
            }));
        } else {
            return Math.min.apply(Math, results.map(function (o) {
                return o.customResult;
            }));
        }
    }

    /**
     * @override
     *
     * Keep a certain number of highest or lowest dice rolls from the result set.
     *
     * 20d20k       Keep the 1 highest die
     * 20d20kh      Keep the 1 highest die
     * 20d20kh10    Keep the 10 highest die
     * 20d20kl      Keep the 1 lowest die
     * 20d20kl10    Keep the 10 lowest die
     *
     * @param {string} modifier     The matched modifier query
     */
    keep(modifier) {
        const rgx = /[kK]([hHlL])?([0-9]+)?/;
        const match = modifier.match(rgx);
        if (!match) return this;
        let [direction, number] = match.slice(1);
        direction = direction ? direction.toLowerCase() : "h";
        number = parseInt(number) || 1;
        TORBaseDie._keepOrDrop(this.results, number, {keep: true, highest: direction === "h"});
    }
}