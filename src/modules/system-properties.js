export const tor1eSystemProperties = {};

/**
 * @see CONFIG.tor1e.properties.rootpath for detail
 * @type {{root: string}}
 */
tor1eSystemProperties.path = {
    "root": "systems/tor1e"
}