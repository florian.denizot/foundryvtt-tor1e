export class Tor1eRollResult {
    nbBonusDice = 0;

    isSuccess = false;

    isFailure = !this.isSuccess;

    static build(type) {
        return {
            result: {
                type: type,
                message: type.message(),
                cssClass: type.css()
            }
        }
    }
}

/**
 * Used to represent a Result hidden from the user
 */
export class Tor1eRollHidden extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 0;

    /** @override */
    isSuccess = false;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.hidden;
    }

    css() {
        return "hidden";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: 0,
            bonus: 0,
            total: 0,
            extra: false,
        };
    }

}

export class Tor1eRollFailure extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 0;

    /** @override */
    isSuccess = false;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.failure;
    }

    css() {
        return "failure";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: 0,
            bonus: 0,
            total: 0,
            extra: false,
        };
    }

}

export class Tor1eRollAutomaticSuccess extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 1;

    /** @override */
    isSuccess = true;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.automaticSuccess;
    }

    css() {
        return "success";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: weaponDamage,
            bonus: 0,
            total: weaponDamage,
            extra: false,
        };
    }

}

export class Tor1eRollSuccess extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 1;

    /** @override */
    isSuccess = true;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.success;
    }

    css() {
        return "success";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: weaponDamage,
            bonus: 0,
            total: weaponDamage,
            extra: false,
        };
    }
}

export class Tor1eRollGreatSuccess extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 2;

    /** @override */
    isSuccess = true;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.greatSuccess;
    }

    css() {
        return "success";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: weaponDamage,
            bonus: actorBonusDamage,
            total: weaponDamage + actorBonusDamage,
            extra: true,
        };
    }
}

export class Tor1eRollExtraordinarySuccess extends Tor1eRollResult {

    /** @override */
    nbBonusDice = 3;

    /** @override */
    isSuccess = true;
    isFailure = !this.isSuccess;

    message() {
        return CONFIG.tor1e.rollResult.extraordinarySuccess;
    }

    css() {
        return "success";
    }

    computeDamage(weaponDamage, actorBonusDamage) {
        return {
            weapon: weaponDamage,
            bonus: actorBonusDamage * 2,
            total: weaponDamage + 2 * actorBonusDamage,
            extra: true,
        };
    }
}