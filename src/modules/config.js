import {tor1eSystemProperties} from "./system-properties.js";

export const tor1e = {};

/**
 * This properties is dynamically set because ti is needed in the Roll Custom Class
 * Tor1eRoll.js  which is evaluated before the config.js is processed.
 * So I need it in an other way.
 * But for the rest of the app, it is mandatory to use it through CONFIG.tor1e.properties.rootpath to avoid cumbersome import.
 * @type {{rootpath: string}} The root path of the system. Use to have loose coupling
 * between code and structure of the app.
 */
tor1e.properties = {
    "rootpath": tor1eSystemProperties.path.root
}

tor1e.stats = {
    "body": "tor1e.stats.body",
    "heart": "tor1e.stats.heart",
    "wits": "tor1e.stats.wits"
}

tor1e.weaponGroups = {
    "tor1e.weapons.groups.none": "tor1e.weapons.groups.none",
    "tor1e.weapons.groups.swords": "tor1e.weapons.groups.swords",
    "tor1e.weapons.groups.axes": "tor1e.weapons.groups.axes",
    "tor1e.weapons.groups.bows": "tor1e.weapons.groups.bows",
    "tor1e.weapons.groups.spears": "tor1e.weapons.groups.spears",
    "tor1e.weapons.groups.bestial": "tor1e.weapons.groups.bestial"
}

tor1e.standardOfLivingGroups = {
    "poor": "tor1e.standardOfLivingGroups.poor",
    "frugal": "tor1e.standardOfLivingGroups.frugal",
    "martial": "tor1e.standardOfLivingGroups.martial",
    "prosperous": "tor1e.standardOfLivingGroups.prosperous",
    "rich": "tor1e.standardOfLivingGroups.rich"
}

tor1e.traitGroups = {
    "speciality": "tor1e.traits.groups.speciality",
    "distinctiveFeature": "tor1e.traits.groups.distinctiveFeature",
    "flaw": "tor1e.traits.groups.flaw"
}

tor1e.rewardTypes = {
    "tor1e.reward.groups.cultural": "tor1e.reward.groups.cultural",
    "tor1e.reward.groups.qualities": "tor1e.reward.groups.qualities"
}

tor1e.virtuesTypes = {
    "tor1e.virtues.groups.cultural": "tor1e.virtues.groups.cultural",
    "tor1e.virtues.groups.masteries": "tor1e.virtues.groups.masteries"
}

tor1e.calledShots = {
    "tor1e.weapons.calledShots.none": "tor1e.weapons.calledShots.none",
    "tor1e.weapons.calledShots.disarm": "tor1e.weapons.calledShots.disarm",
    "tor1e.weapons.calledShots.poison": "tor1e.weapons.calledShots.poison",
    "tor1e.weapons.calledShots.pierce": "tor1e.weapons.calledShots.pierce",
    "tor1e.weapons.calledShots.break-shield": "tor1e.weapons.calledShots.break-shield",
    "tor1e.weapons.calledShots.savage-blow": "tor1e.weapons.calledShots.savage-blow",
    "tor1e.weapons.calledShots.knock-down": "tor1e.weapons.calledShots.knock-down"
}

tor1e.shields = {
    "none": {
        "value": 0,
        "label": "tor1e.items.shields.none"
    },
    "buckler": {
        "value": 1,
        "label": "tor1e.items.shields.buckler"
    },
    "shield": {
        "value": 2,
        "label": "tor1e.items.shields.shield"
    },
    "great-shield": {
        "value": 3,
        "label": "tor1e.items.shields.greatShield"
    },
}

tor1e.skillGroups = {
    "personality": "tor1e.skillGroups.personality",
    "movement": "tor1e.skillGroups.movement",
    "perception": "tor1e.skillGroups.perception",
    "survival": "tor1e.skillGroups.survival",
    "custom": "tor1e.skillGroups.custom",
    "vocation": "tor1e.skillGroups.vocation",
    "combat": "tor1e.skillGroups.combat"
}

tor1e.armourGroups = {
    "leather": "tor1e.armour.groups.leather",
    "mail": "tor1e.armour.groups.mail",
    "head": "tor1e.armour.groups.head",
    "shield": "tor1e.armour.groups.shield"
}

tor1e.callingGroups = {
    "scholar": "tor1e.callings.groups.scholar",
    "wanderer": "tor1e.callings.groups.wanderer",
    "slayer": "tor1e.callings.groups.slayer",
    "warden": "tor1e.callings.groups.warden",
    "treasure-hunter": "tor1e.callings.groups.treasure-hunter",
    "leader": "tor1e.callings.groups.leader"
}

tor1e.constants = {
    reward: "reward",
    virtues: "virtues",
    combat: "combat",
    skill: "skill",
    speciality: "speciality",
    distinctiveFeature: "distinctiveFeature",
    flaw: "flaw",
    trait: "trait",
    armour: "armour",
    mailArmour: "mail",
    leatherArmour: "leather",
    headgear: "head",
    shield: "shield",
    weapon: "weapon",
    hate: "hate",
    specialAbility: "special-ability",
    miscellaneous: "miscellaneous"
}

tor1e.threeStatesCheckbox = {
    0: "empty-square",
    1: "check-square",
    2: "times-square"
}

tor1e.backgroundImages = {
    "miscellaneous": {
        "url": "systems/tor1e/assets/images/icons/gear.png",
        "title": "tor1e.items.miscellaneous.tech.title"
    },
    "weapon": {
        "url": "systems/tor1e/assets/images/icons/weapon_swords.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "armour": {
        "url": "systems/tor1e/assets/images/icons/armour.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "trait": {
        "url": "systems/tor1e/assets/images/icons/distinctive_feature.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "special-ability": {
        "url": "systems/tor1e/assets/images/icons/adversary_special-ability.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "skill": {
        "url": "systems/tor1e/assets/images/icons/skill.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "reward": {
        "url": "systems/tor1e/assets/images/icons/reward.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "virtues": {
        "url": "systems/tor1e/assets/images/icons/virtue.png",
        "title": "tor1e.weapons.details.tech.title"
    },
    "community": {
        "url": "systems/tor1e/assets/images/one-ring.png",
        "title": "tor1e.actors.types.community.title"
    },
    "lore": {
        "url": "systems/tor1e/assets/images/rune-of-gandalf.png",
        "title": "tor1e.actors.types.lore.title"
    },
    "npc": {
        "url": "systems/tor1e/assets/images/icons/distinctive_feature.png",
        "title": "tor1e.actors.types.npc.title"
    },
    "adversary": {
        "url": "systems/tor1e/assets/images/eye-of-sauron.png",
        "title": "tor1e.actors.types.adversary.title"
    },
    "character": {
        "url": "systems/tor1e/assets/images/rune-of-gandalf.png",
        "title": "tor1e.actors.types.character.title"
    },
    "dice-roll": {
        "url": "systems/tor1e/assets/images/icons/miscellaneous/dice-roll.png",
        "title": "tor1e.weapons.details.tech.title"
    }
}

tor1e.rollResult = Object.freeze({
    success: "tor1e.roll.result.success",
    greatSuccess: "tor1e.roll.result.great-success",
    extraordinarySuccess: "tor1e.roll.result.extraordinary-success",
    automaticSuccess: "tor1e.roll.result.automatic-success",
    failure: "tor1e.roll.result.failure",
    automaticFailure: "tor1e.roll.result.automatic-failure",
    hidden: "tor1e.roll.result.hidden",
})
