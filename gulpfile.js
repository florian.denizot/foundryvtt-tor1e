let gulp = require('gulp');
let less = require('gulp-less');

gulp.task('less', function () {
    return gulp
        .src('./src/styles/less/tor1e.less')
        .pipe(less())
        .pipe(gulp.dest('./src/styles'));
});

gulp.task('default', function () {
    return gulp.watch('./src/styles/less/**/*.less', gulp.series('less'));
});